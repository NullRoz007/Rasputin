Rasputin
---
Current version: 2.5.0

Installation
---
Requirements
1. NodeJS + NPM 
2. ffmpeg
3. Probably some other stuff


```
$ git clone https://gitlab.com/NullRoz007/Rasputin.git
$ cd Rasputin
$ npm install

$ cd d2client 
$ npm install
```

Running
---

Requires Rasputin Configuration files & database (config, ras.db), or you can edit dis.js to use your own discord bot id.

```
$ node bot.js
```

Documentation
---
### Adding a module
A basic command module is structured as below, the code is placed inside a js file in the commands folder:
```js
var commands = {
  c_restrictions: [],
  description: "A Basic Command Module",
  commands: {
  	/*
    Your commands here.
    */
  }
}

module.exports = commands;
```

### Adding commands
A command is structured as below:
```js
commands: {
	...
	hopeless: {
		name: 'hello',
    	arg_names: [],
		run: function(args, message, global){
			message.channel.send("Hello, world!");
		},
		help: 'Hello, world!'
	}
	...
}
```

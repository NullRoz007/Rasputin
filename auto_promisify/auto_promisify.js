const util = require('util');
var o = {__promised__ : 1};
const promiser = function(object){
	for(m in object){
		if(typeof(object[m]) == 'function'){
			o[m] = util.promisify(object[m])
		}
		else o[m] = object[m]
	}
	return o;
}

module.exports = promiser;
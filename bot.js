var Sera = require('./server');
var request = require('request');
var encryptor = require('file-encryptor');
var fs = require('fs');
var sqlite3 = require('sqlite3');
var randomstring = require('randomstring');

var args = process.argv.slice(2);
if(args[0] == '-e') {
	fs.unlink('config', (err) => {
		if(err) console.log(err);
		encryptor.encryptFile("config.json", "config", args[1], (err) => {
			if(err) throw err;
			console.log("Encryption Completed.");
		});
	});
} else if(args[0] == '-rd') {
	console.log("!!!WARNING!!! This will reset The Warmind, wiping ALL data.");
	console.log("Do you wish to continue? [ Y/n ]");

	var stdin = process.openStdin();
	stdin.addListener("data", (d) => {
		var result = d.toString().trim();
		if(result == "Y" || result == "y") {
			fs.unlink("./ras.db", (err) => {
				if(err) console.log(err);

				db = new sqlite3.Database('./ras.db');
				db.run(`CREATE TABLE configs (id INTEGER, json TEXT);`, (err) => {
					if(err) throw err;
					db.run(`CREATE TABLE groups (id INTEGER, json TEXT);`, (err) => {
						if(err) throw err;

						var productionBotToken = args[1];
						var devBotToken = args[2];
						var destinyApiToken = args[3];

						console.log("Creating Configuration: ");
						console.log("Production: "+productionBotToken);
						console.log("Development: "+devBotToken);
						console.log("XAPIKEY: "+destinyApiToken);

						var config = JSON.stringify({ "mainBotToken": productionBotToken,"secondaryBotToken": devBotToken,"destinyApiToken": destinyApiToken });
						db.run("INSERT INTO configs (id, json) VALUES (0, '"+config+"')", (err) => {
							if(err) console.log(err);
							db.close();
							process.exit();
						});
					});
				});
	
			});

		} else if(result == "N" || result == "n") {
			console.log("Aborting.");
			process.exit(0);
		} else {
			console.log("Unknown input, Aborting.");
			process.exit(0);
		}
	});
} else {
	//Start the server
	console.log("Starting the server...");
	var Server = new Sera.Server();
	let SeraBot;
	Server.Start();

	console.log("Starting the Discord client...");
	SeraBot = require('./dis');
	
	SeraBot.setToken(randomstring.generate());
	SeraBot.Start(0, "secondary", () => {
		console.log("Startup Completed.");
	});

	Server.on('server-started', (address) => {
		SeraBot.setAddress(address);
	});

	Server.on('api-request', (request) => {
		var method = request.endpoint;
		console.log('Processing: APIREQUEST '+method);
		if(SeraBot == null){
			Server.Respond(request, '{"status": "offline"}');
		} else {
			SeraBot.endpoints[method](request.arguments, (result) => {
				Server.Respond(request, result);
			});
		}
	});

	Server.on('token-update', (token) => {
		SeraBot.setToken(token);
	})

	Server.on('private-api-request', (request) => {
		var method = request.endpoint;
		console.log('Processing: PRIVAPIREQUEST '+method);
		if(SeraBot == null){
			Server.Respond(request, '{"status": "offline"}');
		} else {
			SeraBot.private_endpoints[method](request.arguments, (result) => {
				Server.Respond(request, result);
			});
		}
	});

	Server.on('bnet-auth', (code) => {
		var clientId = "23293";
		var secret = SeraBot.getClientSecret();
		var apiKey = SeraBot.getDestinyApiKey();
		var auth = "Basic "+ new Buffer(clientId+":"+secret).toString('base64');

		var options = {
			url: 'https://www.bungie.net/Platform/App/OAuth/Token/',
			headers: {
				'Authorization': auth,
				'Content-Type': 'application/x-www-form-urlencoded',
			},
			form: {
				'grant_type': 'authorization_code',
				'code': code,
			}
		};
		request.post(options, (err, res, body) => {
			if(err) throw err;
			var json = JSON.parse(body);
			var db = new sqlite3.Database('./ras.db');
			db.all("select * from bnetauth", (err, values) => {
				var allReadyAdded = false;
				for (var v in values){
					var value = values[v];	
					if(value.membershipId == json.membership_id) allReadyAdded = true;s
				}

				if(!allReadyAdded) {
					var sql = "INSERT INTO bnetauth (membershipId, accessToken, refreshToken) VALUES ('"+json.membership_id+"', '"+json.access_token+"', '"+json.refresh_token+"')";
					db.run(sql, (err, result) => {
						if(err) throw err;
						console.log("Added new Auth to RAS DB.");
					});
				} else {
					var sql = "UPDATE bnetauth SET accessToken = '"+json.access_token+"', refreshToken = '"+json.refresh_token+"' WHERE membershipId = '"+json.membership_id+"'";
					db.run(sql, (err, result) => {
						if(err) throw err;
						console.log("Updated Auth for "+json.membership_id);
					});
				}
			});

			
		})
	});
}




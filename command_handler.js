const autopromise = require('./auto_promisify');
const fs = autopromise(require('fs'));
if(!fs.__promised__) console.log("Failed to promisify module: fs");
const path = require('path');
const util = require('util');
const async = require('async');
var helper = require('./helper.js');
var servers = require('./servers.json');
var flat = require("./FlatJS/flat.js");
var CommandContainer = {};

var CommandHandler = function(prefix){
	this.prefix = prefix;
	this.container = CommandContainer;

};

let global;

CommandHandler.prototype.init = function(client, global, folder, callback){
	CommandContainer = {};
	fs.readdir(folder).then((file_list) => {
		for(var f in file_list) {
			if(typeof file_list[f] == 'function') continue;

			var file = file_list[f];
			if(path.extname(file) == ".js"){
				var object = require(folder+'/'+file);
				if(checkForMember('c_restrictions', object)){
					if(checkForMember('description', object)){
						var name = path.basename(file).split('.')[0];
						CommandContainer[name] = object;
						console.log("LOADED: ["+name+"]");
						if(checkForMember('init', object))
						{
							var cLog = console.log;
							console.log = function(text) {
								cLog(name.toUpperCase() + " (init): "+text);
							}
							var moduleInitResult = flat(object.init, [client, global], true);
							if(moduleInitResult) console.log("SUCCESS");
							console.log = cLog;
						}
					}
				} 
			}
		}
		this.container = CommandContainer;
		callback();
	})
	.catch((err) => { console.log("Error: "+err.message+err.stack); });

}

CommandHandler.prototype.handle = function(client, message, g) {
	if(message.content.startsWith(this.prefix)) {
		
		message.content = message.content.replace('“', '"');

		var fixedContent = "";

		var inQuotes = false;

		for(var i = 0; i < message.content.length; i++) {
			var c = message.content[i];
			if(!inQuotes && c == '"') inQuotes = true;
			else if(inQuotes && c == '"') inQuotes = false;
			
			if(i == 0) { fixedContent += c; }
			else {
				if(message.content[i - 1] == ' ' && !inQuotes) {
					if(message.content[i] != ' ') {
						fixedContent += c;
					}
				}
				else fixedContent += c;
			}
		}
		
		message.content = fixedContent;

		//gather server information
		var server_id = message.guild.id;
		var server_config = getServerConfig(server_id);
		console.log("Command from <"+server_config.name+">");
		
		//setup the global object
		global = g;
		global.command_container = CommandContainer;
		global.prefix = this.prefix;
		global.discord_client = client;
		global.server_config = server_config;
		global.servers = servers;

		//get the command string
		var commandName;
		var isMultiWord = false;
		if(checkForMultiWord(message.content.substring(this.prefix.length))){
			commandName = message.content.substring(this.prefix.length).split(' ')[1];
			isMultiWord = true;
		} else {
			commandName = message.content.substring(this.prefix.length).split(' ')[0];
		}

		
		var commandObject = FindCommand(commandName);
		if(!commandObject) { //command wasn't found, start the search
			console.log("Starting search...");
			searchCommands(commandName, message, (suggestions) => {
				if(suggestions.length === 0){
					message.channel.sendMessage("I couldn't find that command or any similar commands.");
					return;
				}
				var output = "Sorry, I couldn't find that command, maybe you meant: \n";
				suggestions.forEach(suggestion => {
					if((typeof suggestion) == "string"){
						output += "`" + this.prefix + suggestion + "`\n";
					}
					else{
						output += "`" + this.prefix + suggestion.name + "`\n";
					}
				});
				message.channel.sendMessage(output);
				console.log("Done, "+suggestions.length + " results.");
			});
		} else {
			var args = message.content.split(' ');
			var commandParentName = args[0].split(global.prefix)[1];
			var restricted = false;

			var container = FindContainer(commandName);
			for(var r in container.c_restrictions) {
				var restriction = container.c_restrictions[r];
				if(restriction[0] + restriction[1] == "=="){
					restriction = restriction.substring(2);
					if(message.channel.name != restriction){
						console.log("Blocked Command because not channel:"+restriction);
						restricted = true;
					}
				}
			
				else {
					if(message.channel.name == restriction){
						console.log("Blocked Command.");
						restricted = true;
					}
				}
			}

			if(commandObject.force_allow){
				for(c in commandObject.force_allow) {
					if(message.channel.name == commandObject.force_allow[c]){
						console.log("Allowed command because force_allow contains: "+message.channel.name);
						restricted = false;
					}
				}
			}

			if(isMultiWord) {
				console.log("Is multi word");
				

				if(commandObject.cat == args[0].split(global.prefix)[1]){
					if(commandObject.cat != commandParentName) restricted = true;
					if(!restricted) {
						args.splice(0, 1);
						args.splice(0, 1);
						commandObject.run(args, message, global);
						
					}
				}
			} else {
				args.splice(0, 1);
				if(!restricted) commandObject.run(args, message, global);	
			}
		}
	}
}

function searchCommands(entry, message, callback){
	var suggestions = [];

	var first = message.content.split(' ')[0].substring(1);
	var name = message.content.split(' ')[1];

	for(cat in CommandContainer) {
		var category = CommandContainer[cat];

		for(com in category.commands){
			var command = category.commands[com];

			if(command.cat && name) {
				if(first != command.cat && name == command.name){

					var distance = levenshteinDistance(first, command.cat);
					if(distance <= 2) { if(suggestions.indexOf(command.cat) == -1) { suggestions.push(command.cat + " " +name); }}
				}
				else if(name != command.name){
					var search = !name ? first : name;
					var distance = levenshteinDistance(search, command.name);
					if(distance <= 2) { if(suggestions.indexOf(command.cat) == -1) { suggestions.push(command.cat + " " + command.name); }}
				}
			}
			else if(command.name && !command.cat){
				var distance = levenshteinDistance(entry, command.name);
				if(distance <= 2) { suggestions.push(command); }
			}
		}
	}
	callback(suggestions);
}

function FindCommand(name){
	console.log("Looking for: <"+name+">");
	for(cat in CommandContainer){
		var category = CommandContainer[cat].commands;
		for(com in category){
			var command = category[com];

			if(command.name == name){
				return command;
			}
		}
	}
}

function FindContainer(name){
	console.log("Finding Container for: <"+name+">");
	for(cat in CommandContainer){
		var category = CommandContainer[cat];
		for(com in category.commands){
			var command = category.commands[com];

			if(command.name == name){
				  return category;
			}
		}
	}
}

function FindContainerName(name){
	for(cat in CommandContainer){
		var category = CommandContainer[cat];
		for(com in category.commands){
			var command = category.commands[com];

			if(command.name == name){
				  return cat;
			}
		}

	}
}

function getServerConfig(server_id){
	for(s in servers.servers){
		var server = servers.servers[s];
		if(server.id == server_id){
			return server;
		}
	}
}

function checkForMultiWord(command){
	var part_one = command.split(' ')[0].trim();
	for(cat in CommandContainer){
		var category = CommandContainer[cat];
		for(com in category.commands){
			var command = category.commands[com];
			if(command.cat){
				if(part_one == command.cat){
					return true;
				}
			}
		}

	}
	return false;
}
module.exports = CommandHandler;

/////////////////////////////////////
//        Helper Functions         //
/////////////////////////////////////

String.prototype.setCharAt = function(idx, chr) {
	if(idx > this.length - 1){
		return this.toString();
	} else {
		return this.substr(0, idx) + chr + this.substr(idx + 1);
	}
};

function checkForMember(member_name, object){
	return toBool( object[member_name] )
}

function toBool(object){
	if(object) { return true; }
	else { return false; }
}

function levenshteinDistance(s, t){
	var matrix = [];
	var n = s.length;
	var m = t.length;
	if(s.length === 0) { return t.length; }
	if(t.length === 0) { return s.length; }
	var i;
	var j;

	for(i = 0; i <= t.length; i++){
		matrix[i] = [i];
	}

	for(j = 0; j <= s.length; j++){
		matrix[0][j] = j;
	}
	for(i = 1; i <= t.length; i++){
		for(j = 1; j <= s.length; j++){
			if(t.charAt(i-1) == s.charAt(j-1)){
				matrix[i][j] = matrix[i-1][j-1];
			} else {
				matrix[i][j] = Math.min(matrix[i-1][j-1] + 1, Math.min(matrix[i][j-1] + 1, matrix[i-1][j] + 1));
			}
		}
	}
	return matrix[t.length][s.length];
}

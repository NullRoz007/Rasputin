const Discord = require('discord.js');

var config = {
  c_restrictions: [],
  description: "Rasputin Config.",
  commands: {
    servers: {
      name: 'servers',
      cat: 'config',
      arg_names: [],
			run: function(args, message, global){
        var servers = global.servers.servers;
        var output = "";

        for(i = 0; i < servers.length; i++){
          var server = servers[i];
          output += i+1+") "+server.name+"\n";
        }

        var output_embed = new Discord.RichEmbed()
            .setTitle("Rasputin's Available Servers")
            .setDescription(output)
            .setFooter("Use !config server <name> to view specific server configuration.");

        message.channel.send(output_embed);
			},
			help: 'Display the available servers.'
    },
    server: {
      name: 'server',
      cat: 'config',
      arg_names: ["server_name"],
      run: function(args, message, global){
        var servers = global.servers.servers;
        for(s in servers){
          var server = servers[s];
          if(server.name == args[0]){
            console.log(server);
            var disabled_modules = "";
            for(m in server.dont_use){
              if(typeof(server.dont_use[m]) == 'function') continue;
              disabled_modules += server.dont_use[m] + "\n";
            }

            if(server.dont_use.length == 0) disabled_modules = "None";
            var embed = new Discord.RichEmbed()
                .setTitle(server.name+" configuration")
                .addField("Disabled Modules", disabled_modules, true)
                .addField("Id", server.id, true);

            message.channel.send(embed);
            return;
          }
        }

        message.channel.send("Unable to find a server with that name.")
      },
      help: 'View configuation for a specific server.'
    }
  }
}

module.exports = config;

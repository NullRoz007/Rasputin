const helper = require("../helper.js");
const Discord = require('discord.js');
const snoo = require('../snoo.js');
const request = require('request');
var async = require('async');
var Snoo = new snoo();

var last_manifest_version = "";
var brayTechKey = "5b983984b5296";
var milestoneDefinitions;
var itemDefinitions;
var vendorDefintions;
var activitiesDefinitions;

var destiny_platforms = {
	'psn': 2,
	'xbox': 1,
	'pc': 4
};

var destiny_lev_encounters = {
	'2693136605' : { level: 300, order: "Gauntlet, Pleasure Gardens, Royal Pools" },	
	'2693136604' : { level: 300, order: "Gauntlet, Royal Pools, Pleasure Gardens" },
	'2693136602' : { level: 300, order: "Pleasure Gardens, Gauntlet, Royal Pools" },
    '2693136603' : { level: 300, order: "Pleasure Gardens, Royal Pools, Gauntlet" },
	'2693136600' : { level: 300, order: "Royal Pools, Gauntlet, Pleasure Gardens"},
	'2693136601' : { level: 300, order: "Royal Pools, Pleasure Gardens, Gauntlet"},
	'1685065161' : { level: 330, order: "Gauntlet, Pleasure Gardens, Royal Pools" }, //330
	'757116822'	: { level: 330, order: "Gauntlet, Royal Pools, Pleasure Gardens" },
	'417231112'	: { level: 330, order: "Pleasure Gardens, Gauntlet, Royal Pools" },
	'3446541099' : { level: 330, order: "Pleasure Gardens, Royal Pools, Gauntlet" },
	'2449714930' : { level: 330, order: "Royal Pools, Gauntlet, Pleasure Gardens"},
	'3879860661' : { level: 330, order: "Royal Pools, Pleasure Gardens, Gauntlet"},
}

destiny = {
	c_restrictions: ["==rasputin"],
	description: "Various Destiny related commands.",
	init: function(client, global, callback) {
		console.log("Downloading Destiny Data...");
		request.get('http://destiny.plumbing/en/raw/DestinyActivityDefinition.json', (err_act, res_act, body_act) => {
			activitiesDefinitions = JSON.parse(body_act);
			callback(true);
		});
	},
	commands: {
		twab:{
			cat: 'destiny',
			name: 'twab',
			force_allow: ["bungie-announcements", "announcements"],
			arg_names: [],
			run: function(args, message, global, callback){
				if(!helper.hasModPerms(message)){
					return;
				}
				var an = helper.getAnnouncementsChannel(message);
				helper.sendNews("destiny", "en", an);
			},
			help: 'Get the latest This Week at Bungie'
		},
		weeklysummary: {
			name: 'weeklysummary',
			cat: 'destiny',
			force_allow: ['bungie-announcements', 'clan-accouncements', 'announcements'],
			arg_names: [],
			run: function(args, message, global , callback) {
				if(!helper.hasModPerms(message)) return;
				console.log("!");
				var destiny = global.destiny;
				var auth = {};

				destiny.GetPublicMilestones({}, (err, milestones) => {
					var embeds = [];
					var milestoneCategories = {
						raids: [],
						weeklies: [],
						clan: [],
						other: []
					};

					async.forEach(milestones, (milestone, c) => {
						if(milestone){
							destiny.GetDestinyEntityDefinition({
								'entityType': 'DestinyMilestoneDefinition',
								'hashIdentifier': milestone.milestoneHash
							}, (err, milestoneData) => {	
								if(milestoneData != null) {
									if(milestoneData.displayProperties != null) {
										if(milestoneData.displayProperties.name.indexOf('Weekly') != -1){
											milestoneCategories.weeklies.push({'def': milestoneData, 'milestone': milestone});
										} else if(milestoneData.displayProperties.name.indexOf('Raid') != -1){
											milestoneCategories.raids.push({'def': milestoneData, 'milestone': milestone});
										} else if(milestoneData.displayProperties.name.indexOf('Clan') != -1) {
											milestoneCategories.clan.push({'def': milestoneData, 'milestone': milestone});
										} else {
											milestoneCategories.other.push({'def': milestoneData, 'milestone': milestone});
										}
									}
								}

								c();
							});
						}
					}, () => {
						var weekliesEmbed = new Discord.RichEmbed();
						var raidsEmbed = new Discord.RichEmbed();
						var clanEmbed = new Discord.RichEmbed();
						var otherEmbed = new Discord.RichEmbed();
						
						function fixText(value) {
							var final = "";
							var counter = 0;
							for(var i = 0; i < value.length; i++){
								var c = value[i];
								if(c == ' ' && counter >= 20) {
									final += '\n';
									counter = 0;
								} else { 
									final += c;
									counter++;
								}
							}

							return final;
						}

						weekliesEmbed.setTitle("Weekly Challenges");
						weekliesEmbed.setThumbnail("https://bungie.net/common/destiny2_content/icons/95ca4f73bad5452e682055592eb7b6cd.png");
						weekliesEmbed.setColor("0x33adff");
						function findRealActivityByDescription(desc){
							for(var def in activitiesDefinitions){
								if(activitiesDefinitions.hasOwnProperty(def)){
									if(activitiesDefinitions[def].displayProperties.description == desc && activitiesDefinitions[def].displayProperties.name != "Nightfall" && activitiesDefinitions[def].displayProperties.name != "Vanguard Strikes") return activitiesDefinitions[def];
								}
							}
						}

						var weekliesRequestOpts = {
							url: "https://api.braytech.org/?request=challenges",
							headers: {
								"X-Api-Key": brayTechKey
							}
						};

						for(var m in milestoneCategories.weeklies) {
							if(milestoneCategories.weeklies.hasOwnProperty(m)) {
								var milestone = milestoneCategories.weeklies[m];
								var description = "*"+milestone.def.displayProperties.description+"*\n";



								if(milestone.milestone.activities){
									var addedActivities = [];

									for(var a in milestone.milestone.activities) {
										if(milestone.milestone.activities.hasOwnProperty(a)){
											var activity = milestone.milestone.activities[a];


											if(activitiesDefinitions[activity.activityHash].displayProperties.name == "Nightfall" || activitiesDefinitions[activity.activityHash].displayProperties.name == "Vanguard Strikes") {
												var realActivity = findRealActivityByDescription(activitiesDefinitions[activity.activityHash].displayProperties.description);
												if(addedActivities.indexOf(realActivity.displayProperties.name) == -1) { 
													description += " ["+realActivity.displayProperties.name+"](https://seraphimelite.com/activity?hash="+realActivity.hash+")";
													description += " [AL: "+activitiesDefinitions[activity.activityHash].activityLevel+"], [PL: "+activitiesDefinitions[activity.activityHash].activityLightLevel+"]\n"; 

													addedActivities.push(realActivity.displayProperties.name); 
												}
											} else {
												if(addedActivities.indexOf(activitiesDefinitions[activity.activityHash].displayProperties.name) == -1) { 
													description += " ["+activitiesDefinitions[activity.activityHash].displayProperties.name+"](https://seraphimelite.com/activity?hash="+activitiesDefinitions[activity.activityHash].hash+")";;
													description += " [AL: "+activitiesDefinitions[activity.activityHash].activityLevel+"], [PL: "+activitiesDefinitions[activity.activityHash].activityLightLevel+"]\n"; 
													
													addedActivities.push(activitiesDefinitions[activity.activityHash].displayProperties.name); 
												}
											}
										}
									}
								}

								weekliesEmbed.addField(milestone.def.displayProperties.name, description);
							}
						}

						//weekliesEmbed.setImage("https://seraphimelite.com/images/weeklychallenges.png");
						message.channel.send(weekliesEmbed);

						raidsEmbed.setTitle("Weekly Raids");
						raidsEmbed.setThumbnail("https://www.bungie.net/common/destiny2_content/icons/6d3201d80cfd37b8771fb8ee718767c3.png");
						raidsEmbed.setColor("0x6B6189");
						for(var m in milestoneCategories.raids) {
							if(milestoneCategories.raids.hasOwnProperty(m)) {
								var milestone = milestoneCategories.raids[m];
								var activity = activitiesDefinitions[milestone.def.activities[0].activityHash];
								 
								raidsEmbed.addField(milestone.def.displayProperties.name, "*Complete Raid: "+activity.displayProperties.name + "*\n\nActivity Level: " + activity.activityLevel + "\nActivity PL: "+activity.activityLightLevel);
							}
						}
						//raidsEmbed.setImage("https://seraphimelite.com/images/weeklyraids2.png");
						message.channel.send(raidsEmbed);
						


						otherEmbed.setTitle("Other Milestones");
						otherEmbed.setThumbnail("https://bungie.net/img/theme/destiny/icons/game_modes/free_roam.png");
						otherEmbed.setColor("0xeab969");
						
						var weekliesRequestOpts = {
							url: "https://api.braytech.org/?request=challenges",
							headers: {
								"X-Api-Key": brayTechKey
							}
						};

						var weekliesVendorOpts = {
							url: "https://api.braytech.org/?request=vendor&hash=863940356",
							headers: {
								"X-Api-Key": brayTechKey
							}
						};

						request(weekliesRequestOpts, (err, res, cBody) => {
							request(weekliesVendorOpts, (err, res, vBody) => {
								var cJson = JSON.parse(cBody);
								var vJson = JSON.parse(vBody);


								for(var entry in cJson.response.data){
									if(cJson.response.data[entry].type == "flashpoint"){
										otherEmbed.addField(cJson.response.data[entry].name, cJson.response.data[entry].description, true);
									}
								}

								function shortenText(string, length){
									var final = "";
									for(var i = 0; i < string.length; i++) {
										if(i < length) final += string[i];
									}
									final += (final.length != string.length) ? "..." : ""
									console.log(final);
									return final; 
								}
								
								var bountiesText = "";
								var bountiesText2 = "";
								var currencyText = "";
								
								var i = 0;
								for(var s in vJson.response.data['863940356'].sales){
									if(vJson.response.data['863940356'].sales.hasOwnProperty(s)){
										var sale = vJson.response.data['863940356'].sales[s];
										
										if(sale.item.itemTypeDisplayName == "Weekly Bounty"){
												
											if(i < 7) bountiesText += "["+sale.item.displayProperties.name.split(': ')[1]+"](http://localhost:8080/item?hash="+sale.item.hash+")\n";
											else bountiesText2 += "["+sale.item.displayProperties.name.split(': ')[1]+"](http://localhost:8080/item?hash="+sale.item.hash+")\n";
											i++;
										} else if(sale.item.displayProperties.name.split(' ')[0] == "Purchase"){
											currencyText += "["+sale.item.displayProperties.name.split("Purchase ")[1]+"](http://localhost:8080/item?hash="+sale.item.hash+")\n";
										}
									}
								}

								otherEmbed.addField("The Spider: Wanted Bounties (1 / 2)", bountiesText, true);
								otherEmbed.addField("(2 / 2)", bountiesText2, true);
								otherEmbed.addField("The Spider: Currency Exchange", currencyText, true);
								message.channel.send(otherEmbed);
							});
						});

						/*for(var m in milestoneCategories.other) {
							if(milestoneCategories.other.hasOwnProperty(m)) {
								var milestone = milestoneCategories.other[m];
								otherEmbed.addField(milestone.def.displayProperties.name, milestone.def.displayProperties.description);
							}
						}*/
						//otherEmbed.setImage("https://seraphimelite.com/images/weeklymilestones2.png");
						
					});
				});
			},
			help: 'Get the weekly summary.'
		},
		xur: {
			name: 'xur',
			cat: 'destiny',
			arg_names: [],
			force_allow: ['bungie-announcements', 'clan-accouncements', 'announcements'],
			run: function(args, message, global, callback){
				var xurOpts = {
					url: "https://api.braytech.org/?request=xur&get=history",
					headers: {
						"X-Api-Key": brayTechKey
					}
				};

				request(xurOpts, (err, res, body) => {
					var xur = JSON.parse(body).response.data;
					var embed = new Discord.RichEmbed();
					embed.setTitle("Xûr - Agent of Nine");
					embed.setDescription("His will is not his own...\n\n**Location**\n"+xur.location.world+" - "+xur.location.region+"\n*"+xur.location.description+"*");
					var itemsText = "";
					for(var i in xur.items){
						if(xur.items.hasOwnProperty(i)){
							var item = xur.items[i];

							itemsText += "["+item.displayProperties.name+"](https://seraphimelite.com/item?hash="+item.hash+")\n";
						}
					}

					embed.addField("Items", itemsText);
					embed.setColor(0xf9e05e);
					embed.setFooter("Data via: https://braytech.org");
					message.channel.send(embed);
				});
			},
			help: 'Get Xur\'s Inventory, !!!WIP!!!'
		}
	}
}


module.exports = destiny;

function kdToColor(kd){
	if(kd <= 0.10) return {code: 0xFF0000, name: 'red'};
	if(kd >= 0.11 && kd <= 0.40) return {code: 0xFFA500, name: 'orange'};
	if(kd >= 0.41 && kd <= 0.99) return {code: 0xFFFF00, name: 'yellow'};
	if(kd >= 1.00) return {code: 0x00FF00, name: 'green'};
}

Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
}

Array.prototype.findByAttribute = function(attr, value) {
    for(var i = 0; i < this.length; i += 1) {
        if(this[i][attr] === value) {
            return i;
        }
    }
    return -1;
}

Object.prototype.toArray = function(){
	var arr = [];
	for(var a in this) {
		if(this.hasOwnProperty(a)) arr.push(this[a]);
	}
	return arr;
}
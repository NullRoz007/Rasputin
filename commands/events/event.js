var helper = require("../../helper");
const Discord = require('discord.js');
const async = require('async');

function createRaidEmbed(destiny, embed, hash, c) {
	destiny.GetDestinyEntityDefinition({
		'entityType': 'DestinyActivityDefinition',
		'hashIdentifier': hash
	}, (err, activity) => {
        console.log(activity);
        embed.setDescription("Activity: " + activity.displayProperties.name + "\n*"+activity.displayProperties.description+"*\n\nRecomended Power Level: "+activity.activityLightLevel);

		embed.setThumbnail("https://www.bungie.net/common/destiny2_content/icons/d742a28698196e5d1e64eab469d74274.png");

        c(embed);
	});
}

function createNightfallEmbed(embed, c) {

} 

function createCrucibleEmbed(embed, c) {

} 

var activityFunctions = {
	"lw": function(des, emb, cb) { createRaidEmbed(des, emb, "1661734046", e => cb(e)); },
    "levi": function(d, e, cb) { createRaidEmbed(d, e, "89727599", e => cb(e)); },
    "sos": function(d, e, cb) { createRaidEmbed(d, e, "119944200", e => cb(e)); },
    "eow": function(d, e, cb) { createRaidEmbed(d, e, "809170886", e => cb(e)); },
    "nf": function(d, e, cb) { createNightfall(e, e => cb(e)); },
    "cr": function(d, e, cb) { createCrucible(e, e => cb(e)); }
};

module.exports = {
    Event: function(id, name, date, time, timezone, platform, creator, server, activityCode){
        this.players = [];
        this.id = id;
        this.name = name;
		this.date = date;
        this.startTime = time;
        this.timeZone = timezone;
        this.creator_id = creator;
        this.platform = platform;
        this.server = server;
        this.rollcallCount = 0;
        this.activityCode = activityCode;
    },
    addPlayer: function(event, player){
        event.players.push(player);
        console.log('Event: '+event.id+", adding player: "+player);
        return event;
    },
    removePlayer: function(event, playerName){
        var index = event.players.findIndex(x => x == playerName);
        if(index != -1){
            event.players.splice(index, 1);
            console.log('Event: '+event.id+", removing player: "+event.players[index]);
        }
        else {
            console.log('Player is not in event');
        }
        return event;
    },
	editTime: function(event, date, time, timezone){
		event.date = date;
		event.startTime = time;
		event.timeZone = timezone;
    return event;
    },
    editName: function(event, name){
        event.name = name;
        return name;
    },
	out: function(event){
        console.log(event);
		return "**" + event.id + " - " +event.name+ "**\n"
				+ "    Joined:** "+ event.players.length.toString() + "**, Date:** "+event.date+"**\n"
				+ "    Start Time:** "+event.startTime + "-"+event.timeZone + "**\n"
				+ "\n";
    },
    createEmbed: function(destiny, event, channel, callback) {
        console.log("Creating embed;")
        const embed = new Discord.RichEmbed()
            .setColor(0x00AE86)
            .setTitle(event.name)
            .addField("ID", event.id)
            .addField("Date", event.date)
            .addField("Platform", event.platform)
            .addField("Start Time", event.startTime + "-" + event.timeZone)
            .setFooter((3 - event.rollcallCount) + "/3 rollcalls remaining.");

        var playerIndex = 1;
        var players = "";

        var classes = [
            "H",
            "W",
            "T" 
        ];

        async.forEach(event.players, (player, callback) => {
            
            console.log(channel);
            helper.getLinker(channel.guild, player, (linker) => {
                if(playerIndex==7) {
                    players+= "Substitutes:\n";
                }
                

                if(linker) {
                    var membershipId = linker.destinyId;
                    var platform = linker.platform;
                    console.log(linker.platform);

                    destiny.GetProfile({
                        membershipType: platform,
                        destinyMembershipId: membershipId,
                        components : 200
                    }, (err, profile)=> {
                        if(err) {
                            console.log(err);
                        
                            players += playerIndex+". "+helper.getUsernameById(event.players[playerIndex - 1], channel)+"\n";
                            ++playerIndex;
                        }

                        else {
                            
                            players += playerIndex+". "+helper.getUsernameById(event.players[playerIndex - 1], channel) + " [";
                            var characterTextArray = [];
                            
                            for(char in profile.characters.data) {
                                if(profile.characters.data.hasOwnProperty(char)){
                                    var character = profile.characters.data[char];
                                    characterTextArray.push(classes[character.classType - 1] + ": "+character.light);
                                }
                            }

                            players += characterTextArray.join(", ")+"]";
                            ++playerIndex;
                        }

                        callback();
                    });
                } else {
                    console.log("no linker!")
                    players += playerIndex+". "+helper.getUsernameById(event.players[playerIndex - 1], channel)+"\n";
                    ++playerIndex;
                    
                    callback();
                }
             });
        }, () => {
            if(players == "") {
                console.log("players is empty!");
                embed.addField("This group is empty.", "use the **+** reaction to join it, or if you are the groups creator you can use the **X** reaction to delete it.");
            }
            else {
                embed.addField("Players", players);
            }
        
            if(event.activityCode != null) {
                activityFunctions[event.activityCode](destiny, embed, (embed) => {
                    callback(embed);
                });
    
            } else {
                callback(embed);
            }
        });
        
    },
    create: function(message, args, tut, callback){
        console.log(message);

        if(args.length < 6) {
            helper.sendCommandError(message, " it's missing/has too many arguments", "r!post \"Example Group\" 11/9 1:00pm NSZT pc");
            return;
        }
    
        if(!tut) {
            if(!helper.hasModPerms(message) && !helper.hasRole(message, "Seraph") && !helper.hasRole(message, "Elite")) {
                message.reply("You need to get a clan tag before you can post your own groups, feel free to ask a mod or join someone else's group.");
                return;
            }
        }
    
        console.log("Creating new event...");
        try
        {
            var name = "";
            var date = "";
            var time = "";
            var timezone = "";
            var plaftorm = "";
    
            var spacesCount = 0;
    
            for(i = 0; i < args.length; i++) {
                args[i] = args[i].replace('“', '"');
            }
            
            if(args[0].indexOf('"') != -1){
                spacesCount = -1;
                var fullName = "";
                var looking = false;
                for(var i = 0; i < args.length; i++){
                    if(args[i].startsWith('"')) { looking = true };
                    if(looking) { 
                        fullName += " "+ args[i]; 
                        spacesCount++;
                        if(args[i].endsWith('"')) break;
                    }
                }
                
                name = fullName.substring(2, fullName.length - 1);
            } else {
                name = args[0];
            }
            
            name = helper.fixString(name);
            date = helper.fixString(args[spacesCount + 1]);
            time = helper.fixString(args[spacesCount + 2]);
            timezone = helper.fixString(args[spacesCount + 3]);
            platform = helper.fixString(args[spacesCount + 4]).toUpperCase();
    
            console.log(name);
            console.log(date);
            console.log(time);
            console.log(timezone);
            console.log(platform);
    
            if(platform != "PC" && platform != "PS4"){
                helper.sendCommandError(message, platform+" is an invalid platform code, valid codes are pc & ps4 or PC & PS4", "r!post \"Example Group\" 11/9 1:00pm NSZT pc");
                return;
            }
    
            helper.getAllGroupsList((groups) => {
                var id = 0;
                var ids = [];
                for(var g in groups) ids.push(groups[g].id);
                ids.sort((a, b) => { return a - b});
                var previousId = -1;
    
                if(groups.length > 0){
                    for(var i = 0; i < ids.length; i++){
                        if(ids[i] != previousId + 1) {
                            id = previousId += 1;
                            break;
                        } else {
                            previousId = ids[i];
                        }
                    }
                }
    
                var newEvent = new this.Event(id, name, date, time, timezone, platform , message.author.id, tut ? null : message.guild.id, null);
                callback(newEvent)
            });
        } catch (err) { throw err; }
    
    }
}

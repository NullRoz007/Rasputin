var helper = require("../helper.js");
var express = require('express');
var request = require('request');
var Events = require('./events/event');
var fs = require('fs');
var async = require('async');

var webhook_server = express();
var webhook_signature;
var client;

general = {
	c_restrictions: ['event-board-pc', 'event-board-ps4'],
	description: "General Commands",
	init: function(c, global, callback){
		client = c;
		callback();
	},
	commands: {
		tutorial: {
			name: 'tutorial',
			arg_names: [],
			run: function(args, message, global) {
				var tutFull = String(fs.readFileSync('tutorial.txt'));

				message.author.send("Starting interactive tutorial...").then(msg => {
					var dmChannel = msg.channel;
					dmChannel.send("Hello, I'm Rasputin. You've requested a tutorial on how to use my LFG features. Would you like an interactive tutorial? (Reply with y / n):");
					var expectedInputs = [
						{ input: "y", func: function(msg, c) {interactive(msg, 0, () => { c() })} }, 
						{ input: "n", func: text }
					];
					
					var tutorialGroup;
					var groupMessage;
					var lastMessage;

					/**
					 * Run a provided step of the tutorial
					 * @param {*} msg the message
					 * @param {*} step the step 
					 * @param {*} cb callback fired when the step is completed
					 */
					function interactive(msg, step, cb) {
						switch (step) {
							case 0:
								expectedInputs = [{ input: 'r!post \"Example Group\" 24/11 1:00pm CST pc', 
													func: function(msg, c) { interactive(lastMessage, 1, function() { c(); }) }}, 
													
												  { input: "r!post \"Example Group\" 24/11 1:00pm CST PC", 
												  	func: function(msg, c) { interactive(lastMessage, 1, function() { c(); }) }}]

								dmChannel.send("Before trying this tutorial I suggest watching this video which includes a quick run down on how to use the bot. https://youtu.be/lOS29A5uAp8?t=352");
								dmChannel.send("First, try creating a new group using the r!post command\nusage: `r!post name date time timezone platform`.\nUse the following information:\n\tName: Example Group\n\tDate: 24/11\n\tTime: 1:00pm\n\tTimezone: CST\t\nPlatform: PC\n(Surround the name in quotes to use multiple words)");
								
								cb();
								break;
							case 1:
								var args = msg.content.split(' ');
								args.splice(0, 1)
								
								tutorialGroup = Events.create(msg, args, true, group => {
									tutorialGroup = group;
									Events.createEmbed(global.destiny, tutorialGroup, dmChannel, event => {
										dmChannel.send(event).then(msg => {
											groupMessage = msg;

											expectedInputs = [{ 
												input: 'r!changename '+tutorialGroup.id + ' Last Wish', 
												func:  function(msg, c) { 
													interactive(lastMessage, 2, () => { 
														c() 
													}); 
												} 
											}];
											
											dmChannel.send("Now, try changing the name of your group to 'Last Wish' using the r!changename command.\nusage: `r!changename id name`\n(No need for quotes with this command)");
											cb();
										});
									});
								});

								break;
							case 2: 
								tutorialGroup.name = "Last Wish";
								Events.createEmbed(global.destiny, tutorialGroup, dmChannel, event => {
									dmChannel.send(event).then(msg => {
										expectedInputs = [{
											input: 'r!changetime 1 23/11 11:00am CST',
											func: (msg, c) => { interactive(lastMessage, 3, () => { c(); }); }
										}];

										dmChannel.send("Good! Now try changing the time of your group to 23/11 11:00am CST using the r!changetime command.\nusage: `r!changetime id date timezone timezone`");
										cb();
									});
								});

								break;
							case 3: 
								tutorialGroup.date = "23/11";
								tutorialGroup.time = "11:00am";

								Events.createEmbed(global.destiny, tutorialGroup, dmChannel, event => {
									dmChannel.send(event).then(msg => {
										expectedInputs = [];
										cb();
									});
								});

								break;
							default:
								console.log("Unknown step: "+step);
								break;
						}
					}

					/**
					 * Send the full tutorial text. 
					 * @param {*} msg 
					 */
					function text(msg) {
						expectedInputs = [];
						dmChannel.send(tutFull);
					}

					/**
					 * handle users messages during the tutorial
					 * @param {*} message 
					 */
					function handler(message) {
						if(message.author.bot || message.channel.id != dmChannel.id) return;

						var found = false;
						var possibleInputTexts = [];
						async.forEach(expectedInputs, (input, c) => {
							possibleInputTexts.push(input.input);

							if(input.input == message.content) {
								lastMessage = message;
								found = true;

								input.func(message, () => {
									c();
								});
							} else {
								c();
							}
						}, () => {
							if(!found) dmChannel.send("Invalid input: "+message.content + "\nI expected: "+possibleInputTexts.join(', or: '));

							if(expectedInputs.length == 0) {
								message.channel.send("Tutorial Completed!\nThe rest of the LFG functionality is provided through four reactions underneath each posted group:\n\t**+** : Add yourself to the group.\n\t**-** : Remove yourself from the group.\n\t**!** : Rollcall the group (Creator only).\n\t**X** : Delete the group (Creator only).\n\nThere are a few other commands that you can use to change your groups, find these using r!help or inputing by 'n' to the first question of this tutorial.");
								client.removeListener('message', handler);
								return;
							}
						});
					}

					client.on('message', handler);
				}).catch(err => { throw err; });
			},
			help: 'run a tutorial on how to use the bots lfg features.'
		},
		ping: {
			name: 'ping',
			arg_names: [],
			run: function(args, message, global){
				message.reply('You called?\nCurrent Version: '+global.VERSION+"\nPing: "+Math.floor(global.discord_client.ping)+"ms.\nUptime: "+msToTime(global.discord_client.uptime)+"\n\n*This bot was made by NullRoz007 and Reusableduckk, @ one of them if there are any problems.*\n\nDonate: ko-fi.com/A8882QT2");
			},
			help: 'Ping the bot'
		},
		changelog: {
			name: 'changelog',
			arg_names: [],
			run: function(args, message, global){
				message.channel.send(global.changelog);
			},
			help: 'Display recent updates/changes to the bot'
		},
		twitch: {
			name: 'twitch',
			arg_names: [],
			run: function(args, message, global){
				helper.getTwitch(message);
			},
			help: 'Get the Seraphime Elite Twitch channel.'
		},
		help: {
			name: 'help',
			arg_names: ['list | category_name (optional)'],
			run: function(args, message, global){
				if(args.length == 0){
					for(m in global.command_container){
						var category = global.command_container[m];
						var name = String(m);
						var skip = false;
						for(m in global.server_config.dont_use){
							var module_name = global.server_config.dont_use[m];
							if(name == module_name) {
								skip = true;
							}
						}
						if(skip) continue;

						var cat_output = "**"+name+" commands**";

						for(c in category.commands){
							var command = category.commands[c];
							if(command.name){
								cat_output += "\n"+global.prefix;

								var usage = global.prefix;
								if(command.cat != undefined){
									cat_output += command.cat + " ";
									usage += command.cat + " "
								}
								usage += command.name;

								command.arg_names.forEach(arg => {
									usage += " <"+arg+"> ";
								});
								cat_output += command.name + " : " + ((command.help != "") ? command.help : "No help text provided") + ", usage: `"+usage+"`";
							}
						}
						message.member.send(cat_output);
					}
					message.reply("Sending you a DM now...");
				}
				else if(args.length == 1) {
					var lookup = args[0];
					if(lookup == "list") {
						var output = "**Avaliable Sections: **\n";
						for(m in global.command_container){
							var category = global.command_container[m];
							output += "**"+m+"**: " +category.description+"\n";
						}
						message.channel.send(output);
					}
					else {
						for(m in global.command_container){
							if(m == lookup){
								var category = global.command_container[m];
								var cat_output = "**"+String(m)+" commands**";
								for(c in category.commands){
									var command = category.commands[c];
									if(command.name){
										cat_output += "\n"+global.prefix;

										var usage = global.prefix;
										if(command.cat != undefined){
											cat_output += command.cat + " ";
											usage += command.cat + " ";
										}

										usage += command.name;
										command.arg_names.forEach(arg => {
											usage += " <"+arg+"> ";
										});
										cat_output += command.name + " : " + command.help + ", usage: `"+usage+"`";
									}
								}
								message.member.send(cat_output);
							}
						}
					}
				}
			},
			help: 'Send this message'
		},
	}
};

module.exports = general;

function msToTime(duration) {
        var milliseconds = parseInt((duration%1000)/100)
        , seconds = parseInt((duration/1000)%60)
		, minutes = parseInt((duration/(1000*60))%60)
        , hours = parseInt((duration/(1000*60*60))%24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    return hours + ":" + minutes + ":" + seconds + "." + milliseconds;
}

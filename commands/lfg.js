const helper = require('../helper');
const Events = require('./events/event');
const Discord = require('discord.js');
const async = require('async');
const EventEmitter= require('events');
const moment = require('moment-timezone');


var groupMessageIds = [];

var destiny;
var channelGen;
var boardPS4;
var boardPC;
var catPS4;
var catPC;

var destiny_platforms = {
	'ps4': 2,
	'xbox': 1,
	'pc': 4
};

class LfgEmitter extends EventEmitter{}
const lfgEmitter = new LfgEmitter();

function getGroupMessages(channel, c) {
	channel.fetchMessages()
	.then(messages => {
		helper.getAllGroupsList((groups) => {
			var fgroups = [];
			for(i = 0; i < messages.array().length; i++) {
				var message = messages.array()[i];
				embed = message.embeds[0];
				var found = false;

				if(embed) {	
					id = parseInt(embed.fields[0].value);
					for(n = 0; n < groups.length; n++){
						if(groups[n].id == id) {
							fgroups.push({msg: message, group: groups[n]});
							found = true;
						}
					}
				}
			}
			c(fgroups); 
		});
	}).catch(error => { c(null); });
}

function getGroupMessage(group, channel, c) {
	getGroupMessages(channel, (groups) => {
		groups.forEach(m => {
			if(m.group.id == group.id) c(m.msg);
		});
	});
}

lfg = {
	c_restrictions: ["==rasputin-commands"],
	description: "Join, create and view groups.",
	init: function(client, global, callback) {
		destiny = global.destiny;

		async.forEach(client.channels.array(), (channel, c) => {		
			if(channel.name == "general") channelGen = channel;
			else if(channel.name == "events-ps4") boardPS4 = channel;
			else if(channel.name == "events-pc") boardPC = channel;
			else if(channel.name == "PS4 Destiny LFG") catPS4 = channel;
			else if(channel.name == "PC Destiny LFG") catPC = channel;
	
			c();
		}, () => {
			helper.getAllGroupsList((groups) => {
				var ids = [];
				for(i = 0; i < groups.length; i++){
					ids.push(groups[i].id);
				}

				function send(event, c){
					Events.createEmbed(destiny, event, channelGen, (embed) => {
						var channel;
						if(event.platform == 'PC') {
							channel = boardPC;
						} else if(event.platform == 'PS4') {
							channel = boardPS4;
						} else {
							console.log("Invalid Group Platform:");
							console.log(event.platform);
						}

						if(channel !== undefined) {
							channel.send(embed).then(message => {
								message.react(helper.getEmoji(message.guild, 'plus')).then(() => {
									message.react(helper.getEmoji(message.guild, 'minus')).then(() => {
										message.react(helper.getEmoji(message.guild, 'rollcall')).then(() => {
											message.react(helper.getEmoji(message.guild, 'delete'));
										});
									});
								});
								c();
							});
						} else {
							c();
						}
					});
				}

				client.on('message', (message) => {
					if(!message.author.bot) {
						if(message.channel.name == 'events-pc' || message.channel.name == 'events-ps4') message.delete();
					}
				});

				client.on('messageReactionAdd', (messageReaction, user) => {
					
					//messageReaction.me always returns true for some reason, even when it's not the client adding the reaction
					if(user.username != 'TestBot' && user.username != 'SeraphimBot Reborn' && (messageReaction.message.channel.name == "events-ps4" || messageReaction.message.channel.name == "events-pc")) { 
						var parentUser = user; //'user' is undefined in getGroupsMessage's callback due to some weird scope issues, this fixes it
						getGroupMessages(messageReaction.message.channel, (groups) => {
							if(groups != null) {
								for(i = 0; i < groups.length; i++){
									if(messageReaction.message.id == groups[i].msg.id) {
										var group = groups[i].group;
										
										if(messageReaction.emoji.name == "plus") {
										
											var event = Events.addPlayer(group, parentUser.id);
											helper.updateGroupsJSON(event);
	
											Events.createEmbed(destiny, group, channelGen, embed => {
												
												messageReaction.message.edit(embed);
											});
										
										} else if(messageReaction.emoji.name == "minus") {
											var event = Events.removePlayer(group, parentUser.id);
											helper.updateGroupsJSON(event);
											Events.createEmbed(destiny, group, channelGen, embed => {
												
												messageReaction.message.edit(embed);
											});
										} else if(messageReaction.emoji.name == "delete") {
											if(parentUser.id == group.creator_id) {
												helper.removeGroup(group.id);
												messageReaction.message.delete().catch(err => { });
											} else if (messageReaction.message.channel.members.find(x => x.user.username == parentUser.username).permissions.hasPermissions([ "MANAGE_MESSAGES", "MANAGE_ROLES_OR_PERMISSIONS" ], true)) {
												helper.removeGroup(group.id);
												messageReaction.message.delete().catch(err => {});
											}
										} else if(messageReaction.emoji.name == "rollcall") {
											if(parentUser.id == group.creator_id && group.rollcallCount < 3) { 
							
												group.rollcallCount += 1;
												helper.updateGroupsJSON(group);
												var collatedPlayers = [];

												for(var playerIndex in group.players) {
													if(group.players.hasOwnProperty(playerIndex)) {
														var playerId = group.players[playerIndex];
														var user = helper.getUserById(playerId, messageReaction.message.channel);

														if(collatedPlayers.indexOf(user) == -1) collatedPlayers.push(user);
													}
												}
												
												for(var u in collatedPlayers) {
													if(collatedPlayers.hasOwnProperty(u)){
														collatedPlayers[u].send("Your group "+group.name+" is starting soon! ("+group.startTime + " " + group.date + " "+group.timeZone+")");
													}
												}

												Events.createEmbed(destiny, group, channelGen, embed => {
													messageReaction.message.edit(embed);
												});
											}
										}
									}
									
								}
							}
						});
					}
				});

				lfgEmitter.on('change', (event) => {
					var channel;
					if(event.platform == "PC") channel = boardPC;
					else channel = boardPS4;
					
					getGroupMessage(event, channel, (message) => {
						Events.createEmbed(destiny, event, channelGen, embed => {					
							message.edit(embed);
						});
					});
				});

				lfgEmitter.on('new', (event) => {
					send(event, () => {  } );
				});

				var missing = [];
				

				async.forEach(groups, (g, c) => {
					var channel = (g.platform == "PS4") ? boardPS4 : boardPC;
					getGroupMessages(channel, (groupMessages) => {
						var idsToCheck = [];
						var found = false;
						for(i = 0; i < groupMessages.length; i++) idsToCheck.push(groupMessages[i].group.id);

						if(idsToCheck.indexOf(g.id) == -1) missing.push(g);
						c();
					});
				}, () => {
					console.log("Sending "+missing.length+" missing group embeds");
					missing.forEach(g => { send(g, () => {}); });
					
					callback();
				});
			});
		});
	},
	commands: {
		post: {
			name: 'post',
			arg_names: ['title', 'date', 'time', 'timezone', 'platform'],
			run: function(args, message, global){
				Events.create(message, args, false, newEvent => {
					Events.addPlayer(newEvent, message.member.id);
					helper.updateGroupsJSON(newEvent);

					lfgEmitter.emit('new', newEvent);
					var boardChannel = message.channel.guild.channels.find((x) => x.name == "events-"+platform.toLowerCase());
					message.reply("your group: "+newEvent.name+" has been created, you can view it in "+boardChannel);
				});

			},
			help: 'Post a new group, surround the title in double quotes to use spaces.'
		},
		setgroupevent: {
			name: 'setgroupactivity',
			arg_names: ['id', 'activitycode'],
			run: function(args, message, global) {
				var eventId = args[0];
				var activityCode = args[1];
				
				helper.getGroup(eventId, group => {

					if(group.creator_id != message.member.user.id ||!helper.hasModPerms(message)){
						return;
					}
					
					group.activityCode = activityCode;
					
					
					helper.updateGroupsJSON(group);
					lfgEmitter.emit('change', group);
				});

			},		
			help: 'Assign a Destiny 2 Activity to a group.'
		},
		link: {
			name: 'link',
			arg_names: ['account_name', 'platform'],
			run: function(args, message, global, callback){
				if(args.length == 2) {
					var platform = args[1];
					console.log(platform);
					var platform_code = destiny_platforms[platform];
					
					if(platform_code == 4){
						args[0] = args[0].replace('#', '%23');
					}


					destiny.SearchDestinyPlayer({
						membershipType: platform_code,
						displayName: args[0]
					}, (err, res) => {
						if(err) console.log(err);
						console.log(res);
						var user = res[0];
						console.log(user);
						if(user){
							helper.getLinksList((linked_users) => {
								for(x = 0; x < linked_users.length; x++){
									var check = linked_users[x].discordId;
									if(message.member.id == check){
										message.channel.sendMessage("Sorry, "+message.member.user.username+" your account is already linked.");
										return;
									}
								}
								var linker = {discordId: message.member.id, destinyId: user.membershipId, token: 'NONE', refreshToken: 'NONE', platform: platform_code};
								helper.updateLinksJSON(linker);
								message.channel.sendMessage("Linked destiny account: "+user.membershipId + " to "+message.member.user.username);
							});
						}
					});
				}
			},
			help: 'Link your destiny account to your discord account.'
		  },
		  unlink: {
			name: 'unlink',
			arg_names: [],
			run: function(args, message, global, callback){
				if(args.length == 0){
					var messageId = String(message.member.id);
					helper.getLinksList((linked_users) => {
						for(i = 0; i < linked_users.length; i++){
							if(String(linked_users[i].discordId) == messageId){
								var user = linked_users[i];

								helper.removeLinkJSON(i);

								message.channel.sendMessage("Unlinked destiny account: "+user.membershipId + " from "+message.member.user.username);

								return;
							}
						}
					});
				}
			},
			help: 'Unlink your destiny account from your discord account'
		},
		addtogroup: {
				name: 'addtogroup',
				arg_names: ['id', '@user'],
				run: function(args, message, global){
					if(!helper.hasModPerms(message)){
						return;
					}
					helper.getGroupsList(message.guild.id, (events) => {
						if (args.length >= 2){

							var id = args[0];

							if (id >= 0){
								var event = events.find(x => x.id == id);

								for(i = 0; i < message.mentions.users.array().length; i++){
									var userID = message.mentions.users.array()[i];
									console.log(userID.id);
									Events.addPlayer(event, userID.id);
									message.channel.send("Added " + userID.username + " to group " + id);
									helper.updateGroupsJSON(event);
									lfgEmitter.emit('change', event);
								}

							} else {
								// Could not find event
								message.channel.send("I could not find that event");
							}
						} else {
							//Missing parameters
							message.channel.send("There are missing parameters. Here is a usage example: \n```!addtogroup 1 Reusableduckk```");
						}
					});
				},
				help: ''
		},
		removefromgroup: {
				name: 'removefromgroup',
				arg_names: ['id', '@user'],
				run: function(args, message, global){
					if(!helper.hasModPerms(message)){
						return;
					}

					if (args.length == 2){
							var id = args[0];
							var userID = message.mentions.users.array()[0];
							helper.getGroupsList(message.guild.id, events => {
								console.log(userID.username);

								if (id >= 0){

									var event = events.find(x => x.id == id);

									Events.removePlayer(event, userID.id);
									message.channel.send("Removed " + userID.username + " from group " + id);
									helper.updateGroupsJSON(event);
									lfgEmitter.emit('change', event);
								} else {
									// Could not find event
									message.channel.send("I could not find that group");
								}
							});
						} else {
							//Missing parameters
							helper.sendCommandError(message, " it's missing arguments", "!removefromgroup 1 Reusableduckk");
							//message.channel.send("There are missing parameters. Here is a usage example: \n```!removefromgroup 1 Reusableduckk```");
						}
				},
				help: ''
		},
		changename: {
			name: 'changename',
			arg_names: ['id', 'name'],
			run: function(args, message, global){
				var id = args[0];
				var name = "";
				for(i = 1; i < args.length; i++){
					name += " " + args[i];
				}

				helper.getGroupsList(message.guild.id, events => {
					if(id >= 0){
						var event = events.find(x => x.id == id);

						if((event.creator_id == message.member.user.id) || helper.hasModPerms(message)){
							Events.editName(event, name);
							message.channel.send("Changed group "+event.id+"'s name to: "+name);

							helper.updateGroupsJSON(event);
							lfgEmitter.emit('change', event);
						} else {
							// mod perms
							message.channel.send("Only the group creator or a Moderator can do that.")
						}
					} else {
						// group not found
						message.channel.send("Group not found.")
					}
				});
			},
			help: 'Change the name of a group. This command assumes that every after the id is the name, no need for quotes.'
		},
		refresh: {
			name: 'refresh',
			arg_names: ['id', 'name'],
			run: function(args, message, global){
				if(!helper.hasModPerms(message)){
					return;
				}

				groupMessageIds = [];
				lfg.init(message.client, () => {
					message.channel.send("Event's channels refreshed.");
				});
			},
			help: 'Refresh the event\'s channels.'
		},
		changetime: {
			name: 'changetime',
			arg_names: ['id', 'date', 'time', 'timezone'],
			run: function(args, message, global){
				if(args.length == 4){
					var id = args[0];
					helper.getGroupsList(message.guild.id, events => {
						if(id >= 0){
							var event = events.find(x => x.id == id);

							if((event.creator_id == message.member.user.id) || helper.hasModPerms(message)){
								Events.editTime(event, args[1], args[2], args[3]);
								message.channel.send(event.name+" time changed to "+args[1] + " " + args[2] + " "+ args[3]);
								helper.updateGroupsJSON(event);
								lfgEmitter.emit('change', event);
							} else {
								message.channel.send("Only the group creator or a Moderator can do that.")
							}
						} else {
							message.channel.send("Group not found.")
						}
					});

				} else {
					helper.sendCommandError(message, " it's missing/too many arguments", "!changetime 2 3/17 8pm EST");
				}
			},
			help: 'Change the date and time of a group'
		}
	}
};

module.exports = lfg;

const helper = require("../helper.js");
var memes = {
  c_restrictions: [],
  description: "Memes",
  commands: {
    phrashing: {
			name: 'phrasing',
			arg_names: [],
			run: function(args, message, global){
				message.channel.send("https://cdn.discordapp.com/attachments/287330746681524234/287332316500459520/f12d01f97d13207582d6962651aee4e5.png");
			},
			help: ''
		},
		hopeless: {
			name: 'hopeless',
			arg_names: [],
			run: function(args, message, global){
				message.channel.send("http://vignette3.wikia.nocookie.net/apocalypse-rising-reawakening/images/e/e9/IT%27S_A_TRAP.jpg/revision/latest?cb=20150523170436");
			},
			help: ''
		},
		monday: {
			name: 'monday',
			arg_names: [],
			run: function(args, message, global){
				message.channel.send("https://cdn.discordapp.com/attachments/288838724249452562/313639589296472064/You_know_why.gif");
			},
			help: ''
		},
		ants: {
			name: 'ants',
			arg_names: [],
			run: function(args, message, global){
				message.channel.send("https://images.discordapp.net/.eJwNyEsOhSAMAMC7cACKVX7ehoekmogQWlfm3V2TWc2j7nGqVe0inVeA7eDcxqZZ2khUNLVGZ0n9YJ1bhSSS8l7LJQwYbHTR-7g4E4yxiF_5eXYTYnDW4GcBun--6n6R-r_AUSGx.VXTb5E1cq6s1mZ92JfmTFoY47Tg");
			},
			help: ''
		},
		opinion: {
			name: 'opinion',
			arg_names: [],
			run: function(args, message, global){
				message.channel.send("https://images.discordapp.net/.eJwNyG0KwyAMANC7eADjrF_pbUTFFmwjJmOwsbtv7-f7qOcaaleHyOQdoJ5caFXNQiv3pjtRHy3Pk3WhC7JILsfVbmGwyWPAGNEFk4zx1v4rblsw3mH0KaF7ILwrjpeed1ffH8McIek.V-qBI3wev4U7dbmXtndJro2ugP8");
			},
			help: ''
		},
		stress: {
			name: 'stress',
			arg_names: [],
			run: function(args, message, global){
				message.channel.send("https://images.discordapp.net/.eJwNyEEOhCAMAMC_8ACKQIX6mwYJmqgQWk-b_fvuHOdj3nmZzRyqQzaA_ZTS525F--RWbeu9XZXHKbb0G1iVy3HXRwV8RlopJYqry86h9_9KISARhogpLJgjvLzMasfTzPcHwfEhxg._jLLjTv7cjzfCxDhFdzJnJe-De4");
			},
			help: ''
		},
		allstar: {
			name: 'allstar',
			arg_names: [],
			run: function(args, message, global){
				message.channel.send("https://www.youtube.com/watch?v=L_jWHffIx5E");
				message.channel.send("Somebody once told me.....");
			},
			help: ''
		},
		benson: {
			name: 'benson',
			arg_names: [],
			run: function(args, message, global){
				message.channel.send("https://www.youtube.com/watch?v=bPFS1iZvlds");
			},
			help: ''
		},
		friday: {
			name: 'friday',
			arg_names: [],
			run: function(args, message, global){
				var today = new Date();

				if (today.getDay() == 5){
					message.channel.send("https://www.youtube.com/watch?v=kfVsfOSbJY0");
				} else {
					message.channel.send("Today is not Friday");
				}
			},
			help: ''
		},
		hepballs: {
			name: 'hepballs',
			arg_names: [],
			run: function(args, message, global){
				message.channel.send("https://youtu.be/Tctfq4tplQ4?t=22s");
			},
			help: ''
		},
		nootnoot: {
			name: 'nootnoot',
			arg_names: [],
			run: function(args, message, global){
				message.channel.send("https://www.youtube.com/watch?v=z3IzWy2RC08");
			},
			help: ''
		},
		varisshittycommand: {
			name: 'vari',
			arg_names: [],
			run: function(args, message, global){
				message.channel.send("https://media.giphy.com/media/8sA7PcyqrarsI/giphy.gif");
			},
			help: ''
		},
		omni: {
			name: 'omni',
			arg_names: [],
			run: function(args, message, global) {
				message.channel.send("https://cdn.discordapp.com/attachments/288838724249452562/401437557046640640/giphy-downsized-large.gif");
			}
		}
    }
}

module.exports = memes;
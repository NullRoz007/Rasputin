const helper = require('../helper.js');
const Discord = require('discord.js');
mod = {
	c_restrictions: [],
	description: 'Mod commands.',
	commands: {
		say: {
			name: 'say',
			arg_names: ['text'],
			run: function(args, message, global){
				if (helper.hasModPerms(message)){
					var output = "";
					args.forEach((p) => {
						output += " "+p
					});
					message.channel.sendMessage(output);
				}
			},
			help: 'Speak as the bot.'
			
		},
		disable: {
			name: 'disable',
			arg_names: ['command_name'],
			run: function(args, message, global){
				if (helper.hasModPerms(message)){
					helper.disableCommand(args[0]);
					message.channel.sendMessage("Disabled command "+args[0]);
				}
			},
			help: 'disable a command'
		},
		enable: {
			name: 'enable',
			arg_names: ['command_name'],
			run: function(args, message, global){
				if (helper.hasModPerms(message)){
					helper.enableCommand(args[0]);
					message.channel.sendMessage("Enabled command "+args[0]);
				}
			},
			help: 'disable a command'
		},
		addrole: {
			name: 'addrole',
			arg_names: [],
			run: function(args, message, global){
				if (helper.hasModPerms(message) || args[0] == 'Raider'){
					if(args.length >= 1){
						
						var foundRole = message.guild.roles.find('name', args[0]);
						var userID = message.mentions.users.array()[0];
						var member = message.member;
						
						if (foundRole != null){

							member.addRole(foundRole.id)
								.then(() => message.channel.send("Added role " + args[0] + " to " + member.user.username))
								.catch(() => message.channel.send("Error adding role, I probably don't have the proper permissions"));
						} else {
							// no role
							message.channel.send("The role: " + args[0] + " does not exist");
						}

					} else {
						// improper syntax
						message.channel.send("Improper syntax. Proper use: !addrole <role> <username>, spaces in the username are okay");
					}
				} else {
					// No mod perms
					message.channel.send("You do not have moderator permissions");
				}
			},
			help: 'Add a role to a user. Parameters: <RoleName> @user'
		},
		removerole: {
			name: 'removerole',
			arg_names: [],
			run: function(args, message, global){
				if (helper.hasModPerms(message)){
					if(args.length >= 1){

						var foundRole = message.guild.roles.find('name', args[0]);
						var userID = message.mentions.users.array()[0];
						var member = message.guild.member(userID);

						if (foundRole != null){

							member.removeRole(foundRole.id)
								.then(() => message.channel.sendMessage("Removed role " + args[0] + " from " + member.nickname))
								.catch(() => message.channel.sendMessage("Error removing role, I probably don't have the proper permissions"));
						} else {
							// no role
							message.channel.sendMessage("The role: " + args[0] + " does not exist");
						}

					} else {
						// improper syntax
						message.channel.sendMessage("Improper syntax. Proper use: !removerole <role> <username>, spaces in the username are okay");
					}
				} else {
					// No mod perms
					message.channel.sendMessage("You do not have moderator permissions");
				}
			},
			help: 'Remove a role from a user. Parameters: <RoleName> @user'
		},
		clear: {
			name: 'clear',
			arg_names: [],
			run: function(args, message, global){

				var amount = args[0];
				if (helper.hasModPerms(message)){

					var msgPromise = message.channel.fetchMessages({limit: amount});

					msgPromise.then(function (pastMsgs) {
						console.log('Finished fetching messages...');
						var promiseArray = pastMsgs.deleteAll();

						console.log(promiseArray.length);

						for (var i = 0; i < promiseArray.length; i++){
							promiseArray[i].then(function (test){
								console.log('Message deleted '+i+'/'+amount);
							});
							promiseArray[i].catch(function (err){
								console.log('FAILURE DELETING MESSAGE', err);
							});
						}
					});

					msgPromise.catch(function (err){
						console.log('WE GOT ERR - 1', err);
					});

				}
				else {
					message.reply('You are not a moderator');
				}
			},
			help: 'Clears the chat. Parameters: <Number of messages to clear> (max 50)'
		},
		fixmygroups: {
			name: 'fixmygroups',
			arg_names: [],
			run: function(args, message, global){
				
				if(!helper.hasModPerms(message)){
					return;
				}
				
				var input = args[0].split('~');
				var before = input[0];
				var after = input[1];
				
				if(input.length == 2){
					console.log("Username change, updating groups..." + before + " => " + after);
					helper.getGroupsList((events) => {
						var user_events = helper.getGroupsForUser(before, events);
						
						console.log(user_events.length);
						
						for(i = 0; i < user_events.length; i++){
							for(j = 0; j < user_events[i].players.length; j++){
								if(user_events[i].players[j] == before){
									user_events[i].players[j] = after;
									console.log("found");
								}
							} 
						}
						helper.refreshGroups(user_events);
						console.log("Groups updated.");
					});
				}	
			},
			help: ''
		}
	}
};

module.exports = mod;

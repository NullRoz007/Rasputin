const Request = require('request');
const Discord = require('discord.js');
const ytdl = require('ytdl-core');
const YouTube = require('youtube-node');
var youtube = new YouTube();


MAX_QUEUE_SIZE = 20;
let queue = [];
let voice_channel;
let dispatcher;
let isReady = true;
let vol = 1;
let c_name = "Music";
youtube.setKey("AIzaSyC-zhcT2QhLhMp1r9lvS6ZkLAYZIJC5q0o");

var music = {
	c_restrictions: ["==rasputin"],
	description: "Music player commands.",
	commands: {
		play: {
			name: 'play',
			arg_names: ["song_name"],

			run: function(args, message, global){
				getVoiceChannel(global.discord_client, voice_channel => {
					if(queue.length == MAX_QUEUE_SIZE) {message.reply("I can't play that song because the queue is full."); return;}
					if(!isReady) {message.reply("Please wait for the next song to load before queuing another one."); return;}
					message.channel.send("Searching, please wait...");
					isReady = false;
					connect(voice_channel, message, global.discord_client, connection => {	
						var suffix = args.join(' ');
						searchVideo(suffix, message, global.discord_client, (result) => {
							isReady = true;
							if(result.response == 'failed'){
								console.log(result.error);
								message.reply("Sorry, I couldn't find that song.");
							}
							else{
								module.exports['_queue'] = queue;
							
							}
						});
					});
				});
			},
			help: 'Play a song from youtube.'
		},
		skip: {
			name: 'skip',
			arg_names: [],
			run: function(args, message, global){
				dispatcher.end();
				
			},
			help: 'Skip the currently playing song.'
		},
		pause: {
			name: 'pause',
			arg_names: [],
			run: function(args, message, global){
				dispatcher.pause();
			},
			help: 'pause the currently playing song.'
		},
		resume: {
			name: 'resume',
			arg_names: [],
			run: function(args, message, global){
				dispatcher.resume();
			},
			help: 'Resume the currently plaused song.'
		},
		clearqueue: {
			name: 'clearqueue',
			arg_names: [],
			run: function(args, message, global){
				queue = [];
				isReady = true;
			},
			help: 'Clear the song queue.'
		},
		queue: {
			name: 'queue',
			arg_names: [],
			run: function(args, message, global){
				var output = "Songs: \n";
				var i = 1;
				
				queue.forEach((song) => {
					if(i == 1) {output += "**Now Playing: **"}
					else {output += i+"): "}
					output += song.snippet.title+"\n";
					i++;
				});

				message.channel.send(output);
			},
			help: 'Display the queue of songs.'
		},
		forceready: {
			name: 'forceready',
			arg_names: [],
			run: function(args, message, global){
				queue = [];
				killConnection(global.discord_client);
				dispatcher = null;
				voice_channel = null;
				isReady = true;
			},
			help: 'Force the music commands to be ready again - only use if music is broken, try and clear the queue first.'
		},
		volume: {
			name: 'volume',
			arg_names: ["+","-"],
			run: function(args, message, global){
				if(vol != 0){
						if(suffix == "-"){
							vol -= 0.1;
						}
						else if(suffix == "+"){
							vol += 0.1;
						}
					}
					message.channel.send("Volume will be set to "+String(Math.floor(vol * 100))+"% for the next song."); 
			},
			help: 'Increase or decrease the volume of music player by 10%'
		}
	}
};

function searchVideo(suffix, message, client, callback){
		console.log("Fetching: "+suffix);
		youtube.search(suffix, 2, (error, result) => {
			if (error || result.items.length == 0) {
				callback({'error': error, response: 'failed'});
				return;
			}
			else {
				queue.push(result.items[0]);
				message.channel.send("Queuing: "+result.items[0].snippet.title)
				callback({response: 'pass', video_title: result.items[0].snippet.title});
				if(queue.length == 1) {runQueue(queue, client)}
			}
		});
	}

function runQueue(queue, client){
	var video = queue[0];
	var BASEURL = "http://www.youtube.com/watch?v=";
	var url = BASEURL+video.id.videoId;
	var connection = client.voiceConnections.first();
	
	
	try{
		var s;
		console.log(url);
		dispatcher = connection.playStream(ytdl(url, {filter: "audioonly"}), { seek: 0, volume: vol });
		dispatcher.on('error', () => {queue.shift(); runQueue(queue, client); isReady = true;});
		dispatcher.on('end', ()=> {queue.shift(); if(queue.length > 0) {runQueue(queue, client); } else { killConnection(client);}});
	}
	catch (err){
		console.log("Error: "+err)
	}
	
}

function killConnection(client){
	console.log("Killing voice connection....");
	var connection = client.voiceConnections.first();
	if(connection){
		dispatcher.end();
		connection.disconnect();
	}
	else if(dispatcher && !connection){
		dispatcher.end();
	}
	else{
		console.log("Unable to kill connection.");
	}
}

function getVoiceChannel(client, c){
	client.channels.forEach(channel => {
		if(channel.type == 'voice'){
			if(channel.name == c_name){
				console.log(channel.name);
				c(channel);
			}
		}
	});
}

function connect(channel, msg, client, c){
	console.log(channel);
	if(client.voiceConnections[0]) { 
		c(client.voiceConnections[0]); 
	}

	channel.join().then(connection => { 
		c(connection); 
		console.log("Connected to voice channel."); 
	}).catch((err) => { console.log(err); });
	
}

function destroy(){
	queue = [];
	killConnection();
}

function flatten(str_array, separator){
	var str = "";
	var i = 0;
	str_array.forEach(s => {
		str += s + separator;
	});
	return str.trim();
}

module.exports = music;
module.exports['_queue'] = queue;
##Usage:
Create promises from the fs module:
```
var auto_promise = require('auto_promisify');
var fs = auto_promise(require('fs'));
fs.readfile("/file.txt")
	.then((data) => { console.log(data); })
	.catch((error) => { console.error(error); })
```
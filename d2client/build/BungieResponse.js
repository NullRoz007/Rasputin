var BungieResponse = function(result){
	if(result.ErrorCode != 1){
		return result;
	}

	if (result.Response && result.Response.data) {
        self = result.Response.data;
    } else if (result.Response) {
        self = result.Response;
    } else {
        self = result;
	}
	self.definitions = {};
	return self;
};

module.exports = BungieResponse;

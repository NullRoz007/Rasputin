module.exports = {
	MissingRequiredParameterInRequestBody: {
		error: true,
		message: "Missing Parameter in Request Body",
		data: {}
	},
	InvalidResponse: {
		message: "Invalid Response from endpoint.",
		data: {}
	},
	UnknownComponent: {
		message: "Unknown component requested",
		data: {}
	}
};
var request = require('request');
var ClientError = require('./ClientError.js');
var BungieResponse = require('./BungieResponse.js');
var util = require('util');
var EventEmitter = require('events').EventEmitter;

class ClientManager extends EventEmitter {
	constructor() {
		super();
		this.base = "";
		this.spec = {};
		this.authorization = {
			headers: {},
			oauth2: {}
		};

		this.definitions = {};
		this.mapping = [];
		this.types = {};
		this.docs = {
			functions: []
		};
		this.enums = {};
	}
};
var client = new ClientManager();

function get_class_names(paths){
	var classes = [];
	for (p in paths){
		if(paths.hasOwnProperty(p)) {
			var path = paths[p];
			var request_type = (typeof path.get !== 'undefined') ? "get" : "post";
			var class_name = path[request_type].operationId.split('.')[0];
			
			if(classes.indexOf(class_name) == -1) classes.push(class_name);
		}
	}
	return classes;
}

function create_types(components, type_class){
	var schemas = components.schemas;
	var responses = components.responses;
	var types = {schema: [], responses: []};

	for (s in schemas){
		if(String(s).split('.')[0] == type_class){
			var type_name = String(s).replace(type_class+".", "");
			types.schema[s] = schemas[s];
		}

	}

	for (r in responses){
		if(String(r).split('.')[0] == type_class){
			var type_name = String(r).replace(type_class+".", "");
			types.responses[r] = responses[r];
		}

	}
	return types;
}

function create_enums(components){
	var enums = {};
	for(s in components.schemas){
		if(components.schemas[s].enum){
			var name = String(s).split('.')[String(s).split('.').length - 1];
			var values = components.schemas[s]['x-enum-values'];
			enums[name] = {};
			for(v in values){
				var value = values[v];

				enums[name][value.identifier] = value.numericValue;
			}
		}
	}
	return enums;
}

function get_function_names(paths, c_name) {
	var functions = [];
	for (p in paths){
		if(paths.hasOwnProperty(p)) {
			var path = paths[p];
			var request_type = (typeof path.get !== 'undefined') ? "get" : "post";
			var function_name = path[request_type].operationId.split('.')[1];
			var class_name = path[request_type].operationId.split('.')[0];

			if(c_name == class_name){
				functions.push(function_name);
			}
		}
	}
	return functions;
}

function build_response_mapping(paths){
	var maps = [];
	for (p in paths){
		if(paths.hasOwnProperty(p)) {
			var map = {
				top: 'response',
				path: p,
				class_name: '',
				type: ''
			};
			var path = paths[p];
			var request_type = (typeof path.get !== 'undefined') ? "get" : "post";
			var responses = path[request_type].responses;
			var ok_response_type = responses['200']['$ref'].split('#/components/responses/')[1];
			map.class_name = path.summary.split('.')[0];
			map.type = (typeof ok_response_type.split('.')[1] === 'undefined') ? ok_response_type : ok_response_type.split('.')[1];
			maps.push(map);
		}
	}
	return maps;
}

function parse_members(object){
	//console.log("In: "+object.constructor.name);

	for(prop in object){
		if(object.hasOwnProperty(prop)){
			if(typeof(object[prop])) parse_members(object[prop]);
		}
	}
}
function handle_result(path, result){
	var bungieResponse = new BungieResponse(result);
	var request_type = (typeof path.get !== 'undefined') ? "get" : "post";
	var path_response = path[request_type].responses['200'];
	//console.log(path_response['$ref']);
	return bungieResponse;
}

function build_function(paths, c_name, f_name){
	for (p in paths){
		if(paths.hasOwnProperty(p)) {
			var path = paths[p];
			if(path.summary.split('.')[1] == f_name){
				var request_type = (typeof path.get !== 'undefined') ? "get" : "post";
	
				client.docs.functions.push({
					'class_name': c_name,
					'name': f_name,
					'description': path[request_type].description,
					'parameters': path[request_type].parameters,
					'request_type': request_type
				});
	
				var url = p;
				var query = [];
				var call = function (params, callback, auth){
					var u = url;
					var p = path;

					console.log(params);

					for (prm in path[request_type].parameters){
						var parameter = path[request_type].parameters[prm];
						console.log("Looking for: "+parameter.name + " in "+parameter.in);
						
						if(parameter.in == "path"){
							if(params[String(parameter.name)]) {
								if(u.includes("{"+parameter.name+"}") == -1) {
									u += params[parameter.name];
								}
								else {
									console.log("Found: "+parameter.name);
									u = u.replace("{"+parameter.name+"}", params[parameter.name]);
								}
							}
						}
						else if(parameter.in == "query"){
							if(params[parameter.name] && query.indexOf(parameter.name + "=" + params[parameter.name]) == -1) query.push(parameter.name + "=" + params[parameter.name]);
						}
					}
	
					var j_char = (query.length > 0) ? '?' : ''
					var options = {
						url: client.base + u + j_char+query.join('&'),
						headers: client.authorization.headers
					};
					
	
					var next = function() {
						if(request_type == "get") {
							console.log(options);
							request(options, (error, response, body) => {
								result = null;
								var handled;
								var error;
								if (error) throw error;
								try {
									result = JSON.parse(body);
									handled = handle_result(p, result);
									
								} catch (e){
									error = e;
								}
	
								callback(error, handled);
							});
						}
						else {
							var request_body_schema = p.post.requestBody.content['application/json'].schema['$ref'];
							var base_name = request_body_schema.split('#/components/schemas/')[1].split('.')[0];
							//(request_body_schema.split('#/components/schemas/')[1]);
		
							var param_names = [];
							for(p_name in params){
								param_names.push(p_name);
							}
		
							options['body'] = {};
							for(t in client.types[base_name].schema){
								if(t == request_body_schema.split('#/components/schemas/')[1]){
									//(client.types[base_name].schema[t].properties);
									for (p_name in client.types[base_name].schema[t].properties) {
										if(param_names.indexOf(p_name) == -1){
											var error = ClientError.MissingRequiredParameterInRequestBody;
											error.data = {
												missing: p_name
											};
		
											callback(error, null);
										}
										else {
											options.body[p_name] = params[p_name]
										}
									}
								}
							}
		
							request.post(options, (error, response, body) => {
								if(error) callback(error);
								try {
									var json = JSON.parse(body);
									var handled = handle_result(json);
									callback(null, handled);
								}
								catch (err){
									callback(err, null);
								}
							});
						}
					}	
					
					if(auth) {
						var newHeaders = {
							'Authorization': "Bearer "+auth.accessToken,
						};
	
						for(h in options.headers) newHeaders[h] = options.headers[h];
						options.headers = newHeaders;
						next();
						
					} else {
						next();
					}
				}
				return call;
			}
		}
	}
}

module.exports = function(specification, valid_names){
	console.log("Building "+specification.info.title + " - "+specification.info.version);
	client.base = specification.servers[0].url;
	var classes = get_class_names(specification.paths);
	client['functions'] = {};
	client.mapping = build_response_mapping(specification.paths);

	for (c in classes){
		var class_name = classes[c];
		client['functions'][class_name] = {};
		var functions = get_function_names(specification.paths, class_name);
		for(f in functions){
			if(functions.hasOwnProperty(f)){
				var func_name = functions[f];
				client['functions'][class_name][func_name] = build_function(specification.paths, class_name, func_name);
			}
		}
	}
	client.enums = create_enums(specification.components);

	for (n in valid_names){
		var name = valid_names[n];
		if(name == "Destiny2") name = "Destiny";
		var types = create_types(specification.components, name);
		client.types[name] = types;
	}

	client.spec = specification;
	return client;
};

const builder = require('./build');
const flatjs = require('./build/FlatJS/flat.js');
const jref = require('json-refs');
const path = require('path');
const fs = require('fs');
const util = require('util');

var exec = require('child_process').exec;
var request = require('request');
var ClientError = require('./build/ClientError.js');
var build_for = [];
var latest_manifest_name;

var setup_backend = function(apikey, cb){
	builder(apikey, build_for, (backend) => {
		cb(backend);
	});
}

var setup_enums = function(client){
	client['Enums'] = client.backend.enums;
	return client;
}
	
var setup_functions = function(client, promisify){
	//console.log(client.backend.functions);
	for (c in client.backend.functions){
		client[c] = {};
		for(f in client.backend.functions[c]){
			client[c][f] = (promisify) ? util.promisify(client.backend.functions[c][f]) : client.backend.functions[c][f];
		}
	}
	return client;
}


/**
 * Class representing the Destiny/BNet Client
 *
 * @class
 * @param {string} api_key A valid Bungie.net API Key.
 * @param {string[]} valid_names A list of Service Names to build against.
 * @param {bool} promisify use a promise based wrapper instead of callbacks.
 * @example
 * const bnet = require('d2client');
 * var Destiny2 = new bnet("api_key", ["Destiny2", "GroupsV2"]);
 * @returns {destiny_client} Returns a new destiny_client
*/
var destiny_client = function(api_key, valid_names, promisify = false, c){
	//console.log(api_key);
	build_for = valid_names;

	setup_backend(api_key, (backend) => {
		this['backend'] = backend;

		destiny_client = setup_functions(this, promisify);
		destiny_client = setup_enums(destiny_client);
		destiny_client.backend.on('error', error => {
			console.log(error.message);
			console.log(error.stack);
		});

		
		c(destiny_client);
	});
	
}


/**
 * Download the Destiny2 Manifest
 * @function
 * @example
 * DestinyClient.downloadManifest((m) => {
	 console.log(m);
 * });
*/
destiny_client.prototype.downloadManifest = function(callback){
	var unzip = require('unzip');
	var req_url = "https://www.bungie.net/platform/destiny2/manifest/";
	var options = {
		headers: {
			'X-API-Key': this.backend.authorization.headers['X-API-Key']
		},
		url: req_url
	};

	request(options, function(error, response, body){
		var Manifest = JSON.parse(body);
		var Ver = Manifest.Response.version;
		var manifest_url = "http://www.bungie.net/"+Manifest.Response.mobileWorldContentPaths.en;

		if(fs.exists(path.basename(manifest_url))) fs.unlink(path.basename(manifest_url, () => {}));
		var download_file = require("download-file");
		var d_options = {
			directory: './',
			filename: path.basename(manifest_url)
		};

		download_file(manifest_url, d_options, (err) => {
			manifest_name = path.basename(manifest_url);
			var stream = fs.createReadStream(manifest_name);
			stream.pipe(unzip.Extract({ path: 'manifest' }));
			stream.on('close', () => {
				latest_manifest_name = manifest_name;
				callback(Manifest.Response);
			});
		});
	});

}

destiny_client.prototype.BuildComponents = function(){
	var requested_components = [];
	for(arg in arguments){
		requested_components.push(arguments[arg]);
	}
	//console.log(requested_components);
	var results_array = [];
	for(c in requested_components){
		var r_comp = requested_components[c];
		var value;
		for(b_comp in this.Enums.DestinyComponentType) {
			if(r_comp == b_comp){
				value = this.Enums.DestinyComponentType[b_comp];
			}
		}
		if(!value) continue;
		
		results_array.push(value);
	}
	return results_array.join(',');
}
/**
 * Query the Destiny2 Manifest
 * @function
 * @param {string} table Name of the table to look in.
 * @param {object} lookup What to search for
 * @example
 * DestinyClient.queryManifest("DestinyInventoryItemDefinition", {"hash": "2179934441"}, (result) => {
 * 		console.log(result);
 * });
 */
destiny_client.prototype.queryManifest = function(table, lookup, callback){
	if(!latest_manifest_name) callback({message: 'Manifest not downloaded.'});
	else {
		var db_path = path.join('./manifest/', manifest_name);
		exec('sqlite-json '+db_path+' --table '+table+' -o '+table+'.json', function (error, stdout, stderr){
			try{
				fs.readFile(table+'.json', (err, data) => {
					if(err) {callback(err); return;}

					var results = [];
					var json_table = JSON.parse(String(data));
					fs.unlink(table+'.json', () => {});
					json_table.forEach(entry => {
						try {
							var json = JSON.parse(entry.json);
							//console.log(json);
							for(m in lookup){
								if(json[m] == lookup[m]){
									callback(json);
								}
							}
						}
						catch(err){
							/*
							Invalid JSON.
							*/
						}
					});
				});
			}
			catch (err){
				callback(err);
			}
		});
	}
}

String.prototype.splice = function(start, delCount, newSubStr) {
    return this.slice(0, start) + newSubStr + this.slice(start + Math.abs(delCount));
};


/**
 * Generate Documentation for the Services.
 * @function
 */
destiny_client.prototype.generateDocs = function(){
	var output = "/*\n"
				+ "Auto Generated.\n*/\n\n"

	var class_names = [];
	for(f in this.backend.docs.functions){
		var func = this.backend.docs.functions[f];
		if(class_names.indexOf(func.class_name) == -1) class_names.push(func.class_name);
	}

	for(i in class_names){
		output += "/**\n*"+class_names[i]+" Service\n* @namespace\n*/\nvar "+class_names[i] + " = function(){};\n";
		output += "/* FUNCTIONS */\n"
		for(f in this.backend.docs.functions){
			if(this.backend.docs.functions[f].class_name == class_names[i]){
				var func = this.backend.docs.functions[f];

				if(func.req_type == "get"){func.description += "\nRequest type: get"}
				else if(func.req_type == "post"){
					func.description += "\nRequest type: post, OAuth Required"
				}

				var params = [];
				for(var p in func.parameters) {
					var param = func.parameters[p];

					var toPush = param.name;
					if(param.schema.type){
						toPush + " : " + param.schema.type;
					} else if(param.schema['$ref']) {
						toPush + " : " + param.schema['$ref'];
					} else {
						toPush + " : " + "string"
					}
					params.push(toPush);
					console.log(param.schema);
				}
				output += `/**
						   * `+func.description+`
						   * @function
						   * @param {object} parameters { `+params.join(', ')+` }
						   */`
				output += class_names[i]+".prototype."+func.name + " = function(parameters, callback){ /*...*/};\n\n";
			}
		}
		output += "\n";
	}

	fs.writeFile("out.js", output, () => {
		exec("jsdoc out.js", (error, stdout, stderr) => {
			if(stderr) console.log(stderr);
			exec("jsdoc client.js", (error, stdout, stderr) => {
				if(stderr) console.log(stderr);
				fs.readFile("out/index.html", (err, data) => {
					if(err) throw err;
					var insert_index = String(data).indexOf("</ul>") + 4;
					var new_index = String(data);
					new_index = new_index.splice(new_index, 0, "test");
					for (i in class_names){
						var class_name = class_names[i];
					}
					fs.writeFile('out/index.html', new_index, () => {});
				});

			});
		});
	});
}

module.exports = destiny_client;

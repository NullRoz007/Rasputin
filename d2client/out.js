/*
Auto Generated.
*/

/**
*Destiny2 Service
* @namespace
*/
var Destiny2 = function(){};
/* FUNCTIONS */
/**
						   * Returns the current version of the manifest as a json object.
						   * @function
						   * @param {object} parameters {  }
						   */Destiny2.prototype.GetDestinyManifest = function(parameters, callback){ /*...*/};

/**
						   * Returns the static definition of an entity of the given Type and hash identifier. Examine the API Documentation for the Type Names of entities that have their own definitions. Note that the return type will always *inherit from* DestinyDefinition, but the specific type returned will be the requested entity type if it can be found. Please don't use this as a chatty alternative to the Manifest database if you require large sets of data, but for simple and one-off accesses this should be handy.
						   * @function
						   * @param {object} parameters { entityType, hashIdentifier }
						   */Destiny2.prototype.GetDestinyEntityDefinition = function(parameters, callback){ /*...*/};

/**
						   * Returns a list of Destiny memberships given a full Gamertag or PSN ID.
						   * @function
						   * @param {object} parameters { displayName, membershipType }
						   */Destiny2.prototype.SearchDestinyPlayer = function(parameters, callback){ /*...*/};

/**
						   * Returns a summary information about all profiles linked to the requesting membership type/membership ID that have valid Destiny information. The passed-in Membership Type/Membership ID may be a Bungie.Net membership or a Destiny membership. It only returns the minimal amount of data to begin making more substantive requests, but will hopefully serve as a useful alternative to UserServices for people who just care about Destiny data. Note that it will only return linked accounts whose linkages you are allowed to view.
						   * @function
						   * @param {object} parameters { membershipId, membershipType }
						   */Destiny2.prototype.GetLinkedProfiles = function(parameters, callback){ /*...*/};

/**
						   * Returns Destiny Profile information for the supplied membership.
						   * @function
						   * @param {object} parameters { components, destinyMembershipId, membershipType }
						   */Destiny2.prototype.GetProfile = function(parameters, callback){ /*...*/};

/**
						   * Returns character information for the supplied character.
						   * @function
						   * @param {object} parameters { characterId, components, destinyMembershipId, membershipType }
						   */Destiny2.prototype.GetCharacter = function(parameters, callback){ /*...*/};

/**
						   * Returns information on the weekly clan rewards and if the clan has earned them or not. Note that this will always report rewards as not redeemed.
						   * @function
						   * @param {object} parameters { groupId }
						   */Destiny2.prototype.GetClanWeeklyRewardState = function(parameters, callback){ /*...*/};

/**
						   * Retrieve the details of an instanced Destiny Item. An instanced Destiny item is one with an ItemInstanceId. Non-instanced items, such as materials, have no useful instance-specific details and thus are not queryable here.
						   * @function
						   * @param {object} parameters { components, destinyMembershipId, itemInstanceId, membershipType }
						   */Destiny2.prototype.GetItem = function(parameters, callback){ /*...*/};

/**
						   * Get currently available vendors from the list of vendors that can possibly have rotating inventory. Note that this does not include things like preview vendors and vendors-as-kiosks, neither of whom have rotating/dynamic inventories. Use their definitions as-is for those.
						   * @function
						   * @param {object} parameters { characterId, components, destinyMembershipId, membershipType }
						   */Destiny2.prototype.GetVendors = function(parameters, callback){ /*...*/};

/**
						   * Get the details of a specific Vendor.
						   * @function
						   * @param {object} parameters { characterId, components, destinyMembershipId, membershipType, vendorHash }
						   */Destiny2.prototype.GetVendor = function(parameters, callback){ /*...*/};

/**
						   * Transfer an item to/from your vault. You must have a valid Destiny account. You must also pass BOTH a reference AND an instance ID if it's an instanced item. itshappening.gif
						   * @function
						   * @param {object} parameters {  }
						   */Destiny2.prototype.TransferItem = function(parameters, callback){ /*...*/};

/**
						   * Extract an item from the Postmaster, with whatever implications that may entail. You must have a valid Destiny account. You must also pass BOTH a reference AND an instance ID if it's an instanced item.
						   * @function
						   * @param {object} parameters {  }
						   */Destiny2.prototype.PullFromPostmaster = function(parameters, callback){ /*...*/};

/**
						   * Equip an item. You must have a valid Destiny Account, and either be in a social space, in orbit, or offline.
						   * @function
						   * @param {object} parameters {  }
						   */Destiny2.prototype.EquipItem = function(parameters, callback){ /*...*/};

/**
						   * Equip a list of items by itemInstanceIds. You must have a valid Destiny Account, and either be in a social space, in orbit, or offline. Any items not found on your character will be ignored.
						   * @function
						   * @param {object} parameters {  }
						   */Destiny2.prototype.EquipItems = function(parameters, callback){ /*...*/};

/**
						   * Set the Lock State for an instanced item. You must have a valid Destiny Account.
						   * @function
						   * @param {object} parameters {  }
						   */Destiny2.prototype.SetItemLockState = function(parameters, callback){ /*...*/};

/**
						   * Insert a plug into a socketed item. I know how it sounds, but I assure you it's much more G-rated than you might be guessing. We haven't decided yet whether this will be able to insert plugs that have side effects, but if we do it will require special scope permission for an application attempting to do so. You must have a valid Destiny Account, and either be in a social space, in orbit, or offline. Request must include proof of permission for 'InsertPlugs' from the account owner.
						   * @function
						   * @param {object} parameters {  }
						   */Destiny2.prototype.InsertSocketPlug = function(parameters, callback){ /*...*/};

/**
						   * Gets the available post game carnage report for the activity ID.
						   * @function
						   * @param {object} parameters { activityId }
						   */Destiny2.prototype.GetPostGameCarnageReport = function(parameters, callback){ /*...*/};

/**
						   * Report a player that you met in an activity that was engaging in ToS-violating activities. Both you and the offending player must have played in the activityId passed in. Please use this judiciously and only when you have strong suspicions of violation, pretty please.
						   * @function
						   * @param {object} parameters { activityId }
						   */Destiny2.prototype.ReportOffensivePostGameCarnageReportPlayer = function(parameters, callback){ /*...*/};

/**
						   * Gets historical stats definitions.
						   * @function
						   * @param {object} parameters {  }
						   */Destiny2.prototype.GetHistoricalStatsDefinition = function(parameters, callback){ /*...*/};

/**
						   * Gets leaderboards with the signed in user's friends and the supplied destinyMembershipId as the focus. PREVIEW: This endpoint is still in beta, and may experience rough edges. The schema is in final form, but there may be bugs that prevent desirable operation.
						   * @function
						   * @param {object} parameters { groupId, maxtop, modes, statid }
						   */Destiny2.prototype.GetClanLeaderboards = function(parameters, callback){ /*...*/};

/**
						   * Gets aggregated stats for a clan using the same categories as the clan leaderboards. PREVIEW: This endpoint is still in beta, and may experience rough edges. The schema is in final form, but there may be bugs that prevent desirable operation.
						   * @function
						   * @param {object} parameters { groupId, modes }
						   */Destiny2.prototype.GetClanAggregateStats = function(parameters, callback){ /*...*/};

/**
						   * Gets leaderboards with the signed in user's friends and the supplied destinyMembershipId as the focus. PREVIEW: This endpoint has not yet been implemented. It is being returned for a preview of future functionality, and for public comment/suggestion/preparation.
						   * @function
						   * @param {object} parameters { destinyMembershipId, maxtop, membershipType, modes, statid }
						   */Destiny2.prototype.GetLeaderboards = function(parameters, callback){ /*...*/};

/**
						   * Gets leaderboards with the signed in user's friends and the supplied destinyMembershipId as the focus. PREVIEW: This endpoint is still in beta, and may experience rough edges. The schema is in final form, but there may be bugs that prevent desirable operation.
						   * @function
						   * @param {object} parameters { characterId, destinyMembershipId, maxtop, membershipType, modes, statid }
						   */Destiny2.prototype.GetLeaderboardsForCharacter = function(parameters, callback){ /*...*/};

/**
						   * Gets a page list of Destiny items.
						   * @function
						   * @param {object} parameters { page, searchTerm, type }
						   */Destiny2.prototype.SearchDestinyEntities = function(parameters, callback){ /*...*/};

/**
						   * Gets historical stats for indicated character.
						   * @function
						   * @param {object} parameters { characterId, dayend, daystart, destinyMembershipId, groups, membershipType, modes, periodType }
						   */Destiny2.prototype.GetHistoricalStats = function(parameters, callback){ /*...*/};

/**
						   * Gets aggregate historical stats organized around each character for a given account.
						   * @function
						   * @param {object} parameters { destinyMembershipId, groups, membershipType }
						   */Destiny2.prototype.GetHistoricalStatsForAccount = function(parameters, callback){ /*...*/};

/**
						   * Gets activity history stats for indicated character.
						   * @function
						   * @param {object} parameters { characterId, count, destinyMembershipId, membershipType, mode, page }
						   */Destiny2.prototype.GetActivityHistory = function(parameters, callback){ /*...*/};

/**
						   * Gets details about unique weapon usage, including all exotic weapons.
						   * @function
						   * @param {object} parameters { characterId, destinyMembershipId, membershipType }
						   */Destiny2.prototype.GetUniqueWeaponHistory = function(parameters, callback){ /*...*/};

/**
						   * Gets all activities the character has participated in together with aggregate statistics for those activities.
						   * @function
						   * @param {object} parameters { characterId, destinyMembershipId, membershipType }
						   */Destiny2.prototype.GetDestinyAggregateActivityStats = function(parameters, callback){ /*...*/};

/**
						   * Gets custom localized content for the milestone of the given hash, if it exists.
						   * @function
						   * @param {object} parameters { milestoneHash }
						   */Destiny2.prototype.GetPublicMilestoneContent = function(parameters, callback){ /*...*/};

/**
						   * Gets public information about currently available Milestones.
						   * @function
						   * @param {object} parameters {  }
						   */Destiny2.prototype.GetPublicMilestones = function(parameters, callback){ /*...*/};

/**
						   * Initialize a request to perform an advanced write action.
						   * @function
						   * @param {object} parameters {  }
						   */Destiny2.prototype.AwaInitializeRequest = function(parameters, callback){ /*...*/};

/**
						   * Provide the result of the user interaction. Called by the Bungie Destiny App to approve or reject a request.
						   * @function
						   * @param {object} parameters {  }
						   */Destiny2.prototype.AwaProvideAuthorizationResult = function(parameters, callback){ /*...*/};

/**
						   * Returns the action token if user approves the request.
						   * @function
						   * @param {object} parameters { correlationId }
						   */Destiny2.prototype.AwaGetActionToken = function(parameters, callback){ /*...*/};



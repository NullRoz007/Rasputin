const Discord = require('discord.js');
const bnet = require('./d2client');
const helper = require('./helper');
const fs = require('fs');
const jimp = require('jimp');
const sqlite = require('sqlite3');
const client = new Discord.Client();
const CommandHandler = require('./command_handler.js');
const async = require('async');
var commandHandler = new CommandHandler('r!');
var config;

var global = {
	VERSION: "2.7",
	discord_client: client,
	changelog: `Changelog:
						**LFG**
						1) Groups can now be assigned a destiny 2 activity
						2) Groups now display players PL and available subclasses
						
						**GENERAL*
						1) Added a r!tutorial command for new users
						`,
	DESTINY_API_KEY: "",
	destiny: null,
	command_container: null,
	command_handler: null,
	prefix: "",
	server_address: "",
	token: "",
};

var endpoints = {
	status: function(args, c){
		console.log(client.status);
		c('{"status": '+client.status+'}');
	},
	changelog: function(args, c) {
		c('{"data": "'+global.changelog+'"}');
	},
	version: function(args, c) {
		c('{"data": "'+global.version+'"}')
	},
	clanmembers: function(args, c){
		console.log(Array.from(client.users.keys()).length)	;
		c('{"data": '+Array.from(client.users.keys()).length+'}');
	},
	groups: function(args, c){
		var groups = [];
		fs.readFile('servers.json', (err, data) => {
			if(err) throw err;
			var servers = JSON.parse(data).servers;
			async.forEach(servers, (server, cb) => {
				helper.getGroupsList(server.id, (g) => {
					if(g.length > 0) groups.push(g);
					cb();	
				});
			}, (err) => {	
				c(JSON.stringify(groups));
			});
		});
	}
};

var private_endpoints = {};

 var dis = {
	Start: function(configId, botName, c){
		db = new sqlite.Database('./ras.db');
		db.get('SELECT * FROM configs WHERE id='+configId, (err, result) => {
			var data = result.json;
			config = JSON.parse(data);

			global.DESTINY_API_KEY = config['destinyApiToken'];
			bnet(global.DESTINY_API_KEY, ["Destiny2", "User"], false, (wrapper) => {		
				global.destiny = wrapper.Destiny2;
				global.bnet = wrapper;
				
				client.login(config[botName+'BotToken']);
				c();
			});
		});
	},
	endpoints: endpoints,
	private_endpoints: private_endpoints,
	setAddress: function(address) {
		global.server_address = address;
	},
	setToken: function(token) {
		global.token = token;
	},
	getClientSecret: function() {
		return config.destinyClientSecret;
	},
	getDestinyApiKey: function() {
		return config.destinyApiToken;
	},
	globalVariables: global 
}

module.exports = dis;

client.on('ready', () => {
	console.log('Client Connected!');
	client.user.setActivity("Ver: " + global.VERSION)
	.then(console.log("Set the game status"))
	.catch(err => console.log(err));

	commandHandler.init(client, dis.globalVariables, "./commands/", () => {
		console.log("Finished loading commands.");
	});
	global.command_handler = commandHandler;
});

client.on('message', function(message) {
	if(message.channel.type != "text" || message.author.bot){
		return;
	}
	try {
		commandHandler.handle(client, message, dis.globalVariables);
	}
	catch (err){
		console.log(err);
	}

});

client.on("guildMemberAdd", (member) => {
	console.log("New Member Joined!");
	console.log(member.user);
	var welMsg = `Welcome ${member.user}! Tell us about yourself.`;
	var dm = "Welcome to Seraphim Elite " + member.user + ", make sure you read the rules in #rules-read-me, and please introduce yourself to the rest of the clan in #general! Make sure you put your PSN in your nickname somewhere (if it isn't already) and let us know where you're from or what timezone you're in. When you're ready, you can set Seraphim Elite as your active clan at: https://www.bungie.net/en/Clan/Detail/1905682";
	member.user.send(dm);

	for(i = 0; i < client.channels.array().length; ++i){
		if(client.channels.array()[i].name == "general")
		{
			client.channels.array()[i].sendMessage(welMsg);
			var initRole = client.channels.array()[i].guild.roles.find('name', 'Initiate');
			
			if(initRole) member.addRole(initRole.id);
		}

	}
});

client.on("guildMemberUpdate", (oldMember, newMember) => {
	// Check for Seraph rank
	if(!helper.userHasRole(oldMember, "Seraph") && helper.userHasRole(newMember, "Seraph")){
		var gen = helper.findChannel(client, "general");

		if(gen != undefined){
			gen.sendMessage("Congratulations " + newMember.user + "! You've got your clan tag!");
		}
	}

	// Check for Initiate rank
	if(!helper.userHasRole(oldMember, "Initiate") && helper.userHasRole(newMember, "Initiate")){
		var ig = helper.findChannel(client, "initiate-grouping");

		if(ig != undefined){
			ig.sendMessage(newMember.user + ", please use this channel to find an Elite to play with. Ping a PC-Vetter, or PS4-Vetter depending on what platform you play.");
		}
	}

	// Check for Visitor rank on Seraphs
	var dm = "Hi, this message to inform you that you have removed from the clan, most likely for inactivity. You will not be removed from discord and you are welcome to reapply at any time. We hope to see you back in our ranks!";

	if(!helper.userHasRole(oldMember, "Visitor") && helper.userHasRole(newMember, "Visitor")){
		if(helper.userHasRole(newMember, "Seraph") && !helper.userHasRole(newMember, "Former Seraph")){

			var g = helper.findChannel(client, "general").guild

			var formerClanRole = g.roles.find('name', 'Former Seraph');
			var seraphRole = g.roles.find('name', 'Seraph');

			newMember.addRole(formerClanRole.id);
			newMember.removeRole(seraphRole.id);

			newMember.user.sendMessage(dm);
		}
	}
});


process.on('unhandledRejection', (reason, p) => {
	console.error(reason, 'Unhandled Rejection at Promise', p);
}).on('uncaughtException', err => {
	console.log(err);
});
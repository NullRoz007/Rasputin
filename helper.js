var request = require('request');
var exec = require('child_process').exec;
var last_manifest_version;
var manifest_name;
const Discord = require('discord.js');
const fs = require('fs');
const path = require('path');
const striptags = require('striptags');
const sqlite3 = require('sqlite3');
var APIKEY = '35f5c306f9954850a9cc2435c85a8d09';

module.exports = {
	/*
	Get a icon for a raid
	*/
	getRaidIcon: function(recruitmentId){
		var i = 0;
		for(i = 0; i < raid_map.length; i++){
			if(raid_map[i].name == recruitmentId){
				return raid_map[i].icon;
			}
		}
	},
	getRaidColor: function(recruitmentId){
		var i = 0;
		for(i = 0; i < raid_map.length; i++){
			if(raid_map[i].name == recruitmentId){
				return raid_map[i].color;
			}
		}
	},
	fixString: function(input){
		if(input == "" || input == " " || input == null){
			return "None Provided.";
		}
		else{
			return input;
		}
	},
	hasModPerms: function(input){
		try{
			var modPerms = [ "MANAGE_MESSAGES", "MANAGE_ROLES_OR_PERMISSIONS" ];
			var mod = input.member.permissions.has(modPerms);
			return mod;
		}
		catch(err){
			console.log(err.message);
		}

	},
	groupHasSherpas: function(group){
		var sherpaRole;
		client.channels.array().forEach((channel) => {
			if(channel.name == "general"){
				sherpaRole = channel.guild.roles.find('name', 'Sherpa');
			}

		});
		console.log(sherpaRole);

		group.players.forEach((player) => {
			var user = findUserNoMsg(player);
			if(user){
				user._roles.forEach((role) => {
					console.log(role + " : "+sherpaRole.id);
					if(role == sherpaRole.id){
						console.log("Found Sherpa.");
						return true;
					}
				});
			}
		});
	},
	random: function(low, high){
		return Math.random() * (high - low) + low;
	},
	fileExists: function(file){
		fs.stat(file, function(err, stat){
			if(err === null){
				return true;
			}
			else {
				return false;
			}
		});
	},
	allAre(list, value){
		for(i = 0; i < list.length; i++){
			if(list[i] != value){
				return false;
			}
		}
		return true;
	},
	hasRole: function(message, roleName){
		return message.member.roles.exists('name', roleName);
	},
	userHasRole(member, roleName){
		return member.roles.exists('name', roleName);
	},
	getUsernameById(id, channel){
		console.log(id);

		var member = channel.guild.members.find(val => val.id == id);
		if(member){
			if(member.nickname == null){
				return member.user.username;
			}

			return member.nickname;
		} else {
			return "Invalid User";
		}
	},
 getUserById(id, channel){
		var member = channel.guild.members.find(val => val.id == id);
		if(member){
			return member.user;
		}
		return "Invalid User";
	},
	getId: function(input){
		return input.id;
	},
	/*
	Check if a user is a bot commander
	*/
	isBotCommander: function(input){
		return input.member.roles.exists('name', 'Bot Commander');
	},
	/*
	Send a command error
	*/
	sendCommandError: function(message, issue, example){
		message.channel.sendMessage("I was unable to process the command: ```diff\n"+message.content+"``` Because "+issue+". Here is an example:\n```\n"+example+"\n```");
	},
	/*
	Setup the destiny manifest
	*/
	setupManifest: function(message, callback){
		var unzip = require('unzip');
		var parent = this;
		var req_url = "https://www.bungie.net/platform/destiny2/manifest/";
		var options = {
			headers: {
				'X-API-KEY': APIKEY
			},
			url: req_url
		};

		request(options, function(error, response, body){
			var Manifest = JSON.parse(body);
			var Ver = Manifest.Response.version;

			if(Ver != last_manifest_version){
				console.log("Version mismatch, updating...");

				message.channel.sendMessage("Updating Manifest...");
				var manifest_url = "http://www.bungie.net/"+Manifest.Response.mobileWorldContentPaths.en;
				manifest_name = path.basename(manifest_url);
				var download = require('download-file');
				var download_options = {
					directory: './manifest/',
					filename: 'unextracted_manifest.zip'
				};
			
				var finalize = function(){
					var unzip = require('extract-zip');
					unzip('manifest/unextracted_manifest.zip', {dir: __dirname+'/manifest'}, (err) => {
						if(err) throw err;
						callback();
					});
				}
				download(manifest_url, download_options, (err) => { finalize() })
			}
			else{
				callback();
			}
		});

	},
	getTableFromManifest: function(table, callback){
		
		var db_path = path.join('./manifest/', manifest_name);
		var db = new sqlite3.Database(db_path);
		db.all("select * from "+table, (err, entries) => {
			if(err) throw err;
			callback(entries);
		});
	},
	getTablesFromManifest: function(tables, callback){
		var i = 0;
		var return_tables = [];
		for(t in tables){
			this.getTableFromManifest(tables[t], (table) => {
				return_tables.push(table);
				if(i == tables.length){
					callback(return_tables);
				}
			});
		}
	},
	queryManifest: function(table, hash, callback){
		this.getTableFromManifest(table, () => {
			for(e in table){
				try{
					var json = JSON.parse(table[e].json);
					if(json.hash == hash) callback(json);
				}
				catch (err){}
			}
		});
	},
	/*
	Return a count of members in an object
	*/
	countMembers: function(object){
		var i = 0;
		for(prop in object){
			if(object.hasOwnProperty(prop)){
				i++;
			}
		}
		return i;
	},
	/*
	Get a discord <-> destiny linker for a discord accout
	*/
	getLinker: function(server, discordId, callback){
		var found = false;
		this.getLinksList((linked_users) => {
			for(i = 0; i < linked_users.length; i++){
				if(linked_users[i].discordId == discordId){ 
					var linker = linked_users[i];
					if(!linker.platform){
						console.log("Using default platform.");
						linker['platform'] = server.platform;
					}
					found = true;
					callback(linker);
				}
			}
			if(!found) callback(null);
		});
	},
	/*
	Get groups for a specific user
	*/
	getGroupsForUser: function(id, events){
		var return_events = [];
		for(i = 0; i < events.length; i++){
			var event = events[i];
			if(event.players.contains(id)){
				return_events.push(event);
			}

		}
		return return_events;
	},
	/*
	Get the discord announcements channel
	*/
	getAnnouncementsChannel: function(message){
		var channels = message.guild.channels.array();
			for(i = 0; i < channels.length; i++){
				if(channels[i].name == 'bungie-announcements'){
					return channels[i];
				}
			}
	},
	findChannel(client, chname){
		for(i = 0; i < client.channels.array().length; ++i){
			if(client.channels.array()[i].name == chname)
			{
				return client.channels.array()[i];
			}
		}
		return undefined;
	},
	/*
	Get the Seraphim Elite twitch channel
	*/
	getTwitch: function(message, channel){

		var baseoptions = {
			url: 'https://api.twitch.tv/kraken/streams/seraphimelite1',
			headers: {
				'Accept': 'application/vnd.twitchtv.v3+json',
				'Client-ID': '4nl43m7wvaltcly6fsm921811950ij'
			}
		};

		request(baseoptions, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				var channel = JSON.parse(body);
				var stream = channel.stream;
				var embed = new Discord.RichEmbed()
					.setTitle("Seraphim Elite Twitch Channel");
				if (stream === null) {
					var channeloptions = {
						url: 'https://api.twitch.tv/kraken/channels/seraphimelite1',
							headers: {
								'Accept': 'application/vnd.twitchtv.v3+json',
								'Client-ID': '4nl43m7wvaltcly6fsm921811950ij'
							}
					};

					request(channeloptions, function (error, response, body){
						var whole_channel = JSON.parse(body);
						var id = whole_channel._id;
						console.log(id);

						var host_options = {
							url: "http://tmi.twitch.tv/hosts?include_logins=1&host="+id
						};
						request(host_options, function(error, response, body){
							var hostedValue = JSON.parse(body);
							console.log(hostedValue);
							if(hostedValue.hosts[0].host_login == hostedValue.hosts[0].target_login){
								embed.setDescription("No one is streaming currently... \nhttps://www.twitch.tv/seraphimelite1");
								embed.setThumbnail("https://static-cdn.jtvnw.net/jtv_user_pictures/seraphimelite1-profile_image-4830ebfdb113f773-300x300.png");
								embed.setURL("https://www.twitch.tv/seraphimelite1");
								message.channel.sendEmbed(embed);
							}
							else{
								embed.setDescription("Hosting: "+hostedValue.hosts[0].target_display_name+"\nhttps://www.twitch.tv/"+hostedValue.hosts[0].target_login);
								embed.setThumbnail("https://static-cdn.jtvnw.net/jtv_user_pictures/seraphimelite1-profile_image-4830ebfdb113f773-300x300.png");
								var hostedoptions = {
									url: 'https://api.twitch.tv/kraken/streams/'+hostedValue.hosts[0].target_login,
										headers: {
											'Accept': 'application/vnd.twitchtv.v3+json',
											'Client-ID': '4nl43m7wvaltcly6fsm921811950ij'
										}
									};
								request(hostedoptions, function(error, response, body){
									var channel = JSON.parse(body);
									var image_link = channel.profile_banner;
									console.log(">"+channel.stream);
									if(channel.stream == null){
										embed.setDescription("No one is streaming currently... \nhttps://www.twitch.tv/seraphimelite1")
										embed.setThumbnail("https://static-cdn.jtvnw.net/jtv_user_pictures/seraphimelite1-profile_image-4830ebfdb113f773-300x300.png");
										embed.setURL("https://www.twitch.tv/seraphimelite1");
										message.channel.sendEmbed(embed);
										return;
									}
									//console.log("OH OH");
									console.log(channel.stream.preview.large);
									embed.setImage(channel.stream.preview.large);
									message.channel.sendEmbed(embed);
								});

							}
						});
					});

				}
				else {
					console.log(stream);
					var title = stream.channel.status;
					embed.setTitle(title);
					embed.setDescription("Game: "+stream.game+"\nhttps://www.twitch.tv/seraphimelite1");
					embed.setImage(stream.preview.large);
					embed.setThumbnail("https://static-cdn.jtvnw.net/jtv_user_pictures/seraphimelite1-profile_image-4830ebfdb113f773-300x300.png");
					embed.setURL("https://www.twitch.tv/seraphimelite1");
					message.channel.sendEmbed(embed);
				}

			}
			else if (!error) {
				console.log(response);
			}
		});

	},
	getDisabledCommands: function(callback){
		fs.readFile('com_config.json', (error, data) => {
			if (error) console.log(error);
			callback(JSON.parse(String(data))['disabled_commands']);
		});
	},
	disableCommand: function(name){
		fs.readFile('com_config.json', (error, data) => {
			if (error) console.log(error);
			var jobj = JSON.parse(String(data));
			jobj.disabled_commands.push(name);
			fs.writeFile('com_config.json', JSON.stringify(jobj));
		});
	},
	enableCommand: function(name){
		fs.readFile('com_config.json', (error, data) => {
			if (error) console.log(error);
			var jobj = JSON.parse(String(data));
			var new_commands = [];
			jobj.disabled_commands.forEach((c) => {
				if(c != name) new_commands.push(c)
			});
			jobj.disabled_commands = new_commands;
			fs.writeFile('com_config.json', JSON.stringify(jobj));
		});
	},
	/*
	Send news from bungie.net
	*/
	getDestinyEnums: function(callback){
		request('https://raw.githubusercontent.com/DestinyDevs/BungieNetPlatform/master/wiki-builder/data/enums.json', (error, response, body) => {
			if(error) throw error;
			var enums = JSON.parse(body);
			callback(enums);
		});
	},
	getEmoji: function(guild, name){
		var e = guild.emojis.find((x) => x.name == name);
		return e;
	},
	sendNews: function(type, lang, channel){
		var base_options = {
			url: 'https://www.bungie.net/Platform/Content/Site/News/'+type+"/"+lang+"/",
			headers: {
				'X-API-KEY': APIKEY
			}
		}
		console.log(base_options.url);
		request(base_options, function(error, response, body){
			if(!error && response.statusCode == 200){
				var response = JSON.parse(body);
				var twab = response.Response.results[0];

				var title = twab.properties.Title + " - "+twab.author.displayName;
				var sub = twab.properties.Subtitle;
				var banner = twab.properties.FrontPageBanner;
				var link = "http://www.bungie.net/en/News/Article/"+twab.contentId+"/";
				var content_prev = striptags(twab.properties.Content).slice(0, 245)+"...";
				console.log(title+"\n"+sub+"\n"+banner);

				var embed = new Discord.RichEmbed()
					.setTitle(title)
					.setDescription(sub+"\n\n"+content_prev)
					.setImage("http://www.bungie.net"+banner)
					.setURL(link)
					.setColor(0x5182d1)
					.setThumbnail("http://www.bungie.net/img/profile/avatars/shield2.jpg");
				channel.sendEmbed(embed);
			}
			else{
				console.log(error);
			}
		});

	},
	secondsToTime: function(secs)
	{
	    var hours = Math.floor(secs / (60 * 60));

	    var divisor_for_minutes = secs % (60 * 60);
	    var minutes = Math.floor(divisor_for_minutes / 60);

	    var divisor_for_seconds = divisor_for_minutes % 60;
	    var seconds = Math.ceil(divisor_for_seconds);

	    var obj = {
	        "h": hours,
	        "m": minutes,
	        "s": seconds
	    };
	    return obj;
	},

	isNumber: function(n){
		return !isNaN(parseFloat(n)) && isFinite(n);
	},
	/*
	Get and the list of groups from the events.json file
	*/
	getGroupsList: function(server_id, callback){
		fs.exists("home/events.json", function(exists){
			if(exists){
				fs.readFile('home/events.json', (err, data) => {
					try{
						var jObject = JSON.parse(data);
						var groups = [];

						jObject.forEach(group => {
							if(group.server == server_id) groups.push(group);
						});

						callback(groups);
					}
					catch(err){
						console.log(err);
					}

				});

			}
			else{
				console.log("Event file does not exist.");
			}

		});
	},
	getAllGroupsList: function(callback){
		fs.exists("home/events.json", function(exists){
			if(exists){
				fs.readFile('home/events.json', (err, data) => {
					try{
						var jObject = JSON.parse(data);
						callback(jObject);
					}
					catch(err){
						console.log(err);
					}

				});

			}
			else{
				console.log("Event file does not exist.");
			}

		});
	}, //update
	getGroup: function(id, callback){
		fs.exists("home/events.json", function(exists){
			if(exists){
				fs.readFile('home/events.json', (err, data) => {
					try{
						var jObject = JSON.parse(data);
						for(var g in jObject){
							if(jObject.hasOwnProperty(g)){
								var group = jObject[g];
								if(group.id == id) callback(group);
							}
						}
					}
					catch(err){
						console.log(err)
					}
				});
			}
			else{
				console.log("Event file does not exist.");
			}

		});
	},
	removeGroup: function(id){
		fs.exists("home/events.json", function(exists){
			if(exists){
				fs.readFile('home/events.json', (err, data) => {
					try{
						var jObject = JSON.parse(data);
						
						for(i = 0; i < jObject.length; i++){
							if(jObject[i].id == id) {
								jObject.splice(i, 1);
							}
						}
						fs.unlink('home/events.json', (err) => {
							if(!err) fs.writeFile('home/events.json', JSON.stringify(jObject), (err) => { if(err) console.log(err); });
							else console.log("Unable to write changes to events.json:\n"+err);
						});
					}
					catch(err){
						console.log(err)
					}

				});

			}
			else{
				console.log("Event file does not exist.");
			}

		});
	},
	getLinksList: function(callback) {
		fs.exists("home/links.json", function(exists){
			if(exists){
				fs.readFile('home/links.json', (err, data) => {
					var arrayObject = JSON.parse(data);
					callback(arrayObject);
				});

			}
			else{
				console.log("Link file does not exist.");
			}

		});
	},
	updateLinksJSON: function(linker) {
		this.getLinksList((linked_users) => {
				console.log('Updating links JSON, adding link');
				try{
					linked_users.push(linker);
					var jsonString = JSON.stringify(linked_users);
					fs.writeFile("home/links.json", jsonString);
				}
				catch(err){
					console.log(err);
				}

		});
	},
	removeLinkJSON: function(index) {
		this.getLinksList((linked_users) => {
				console.log('Updating links JSON, removing link');
				try{
					linked_users.splice(index, 1);
					var jsonString = JSON.stringify(linked_users);
					fs.writeFile("home/links.json", jsonString);
				}
				catch(err){
					console.log(err);
				}

		});
	},
	findUser: function(name, client){
		var g = null;
		for(i = 0; i < client.channels.array().length; ++i){
			if(client.channels.array()[i].name == "general")
			{
				g = client.channels.array()[i].guild;
			}

		}
		var memberList = g.members.array();
		var foundMember = null;

		var username = null;
		var nickname = null;

		for (var i = 0; i < memberList.length; i++){

			if (memberList[i].nickname !== null){
				nickname = memberList[i].nickname.trim();
			} else {
				nickname = null;
			}

			if (memberList[i].user.username !== null){
				username = memberList[i].user.username.trim();
			} else {
				username = null;
			}

			if (memberList[i].nickname == name.trim()){
				foundMember = memberList[i];
			} else if (memberList[i].user.username == name.trim()){
				foundMember = memberList[i];
			}
		}
		if (foundMember != null){
			console.log("Found user");
		} else {
			console.log("Could not find user");
		}
		return foundMember;
	},
	updateGroupsJSON: function(event){
		fs.exists("home/events.json", function(exists){
			if(exists){
				fs.readFile('home/events.json', (err, data) => {
					try{
						var jObject = JSON.parse(data);
						if(eventExists(event, jObject)){
							for(var i = 0; i < jObject.length; i++){
								if(jObject[i].id == event.id) {
									jObject[i] = event;
								}
							}
						}
						else {
							jObject.push(event);
						}

						var lStr = JSON.stringify(jObject);
						fs.writeFile("home/events.json", lStr, (err) => { if(err) console.log(err); });
					}
					catch(err){
						console.log(err)
					}
				});
			} else {
				console.log("Event file does not exist.");
			}
		});
	},
	getGroupsLength: function(callback){
		fs.exists("home/events.json", function(exists){
			if(exists){
				fs.readFile('home/events.json', (err, data) => {
					try{
						var jObject = JSON.parse(data);
						callback(jObject.length);
					}
					catch(err){
						console.log(err)
					}

				});

			}
			else{
				console.log("Event file does not exist.");
			}

		});
	}
}

Array.prototype.contains = function(obj) {
    var i = this.length;
    while (i--) {
        if (this[i] === obj) {
            return true;
        }
    }
    return false;
}

function eventExists(event, list){
		for(i = 0; i < list.length; i++){
				var e = list[i];
				if(e.id == event.id){
					return true;
				}
		}
		return false;
}

const fs = require('fs');
var express = require('express');
var os = require('os');
var bodyParser = require('body-parser');
var path = require('path');
var querystring = require('querystring');
var sha1 = require('sha1');
var util = require('util');
var https = require('https');
var request = require('request');
var sha256 = require('sha256');
var sqlite3 = require('sqlite3');
var EventEmitter = require("events").EventEmitter;
var util = require('util');
var encryptor = require('file-encryptor');
var randomstring = require('randomstring');
var cookieSession = require('cookie-session');
var app = express();
app.use(express.static('./www/'));
app.use(cookieSession({
	name: 'session',
	keys: ['key1', 'key2'],
}));


var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
}


var Server = function(){
	var self = this;	
	this['accessToken'] = '';
	EventEmitter.call(this);
}

Server.prototype.__proto__ = EventEmitter.prototype;
Server.prototype.Respond = function(request, data){
	var res = request.res;
	res.end(data);
}

Server.prototype.OAuthHandler = function() {
	console.log("!");
}

Server.prototype.Start = function() {
	var self = this;
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({extended: true}));
	app.use('/home', express.static('/home'));
	app.use(allowCrossDomain);	

	var port = 8080;
	var isWin = /^win/.test(process.platform);
	if(isWin) port = 8080;
	var server = app.listen(port, function() {
		console.log("Listening on port: %s", server.address().port);
		
	});
	
	app.get('/api/:endpoint', (req, res) => {
		var endpoint = req.params.endpoint;
		if(!endpoint){
			res.end('{"error": "incorrect request"}');
		} else {
			var req_payload = {
				'res': res,
				'endpoint': endpoint,
				'arguments': req.query['arguments']
			};
			self.emit('api-request', req_payload);
		}
	});

	app.post('/api/private/:endpoint', (req, res) => {
		var endpoint = req.params.endpoint;
		if(req.session.username & req.session.password != null){
			res.send("Worked.");
		} else {
			var db = new sqlite3.Database('./ras.db', (err) => { if(err) throw err; });
			var q = "SELECT * FROM users";
			db.all(q, (err, entries) => {
				for(var i = 0; i < entries.length; i++){
					var entry = entries[i];
					if(entry.username == req.body.username && sha256(req.body.password).toUpperCase() == entry.pword_sha256){
						var req_payload = {
							'res': res,
							'endpoint': req.params.endpoint,
							'arguments': req.body
						};
						self.emit('private-api-request', req_payload);
					}
				}
			});
		}
	});

	app.get('/groups', (req, res) => {
		res.sendFile(__dirname+"/www/groups.html");
	});

	
	app.get('/wm', (req, res) => {
		res.sendFile(__dirname+"/www/welcomemessage.html");
	});

	app.get('/auth', (req, res) => {
		console.log("AHH");
		self.emit('bnet-auth', req.query['code']);
		res.send("Authenticated!");
	});

	app.get('/item', (req, res) => {
		res.sendFile(__dirname+"/www/item.html");
	});

	app.get('/activity', (req, res) => {
		res.sendFile(__dirname+"/www/activity.html");
	});
}

module.exports.Server = Server;
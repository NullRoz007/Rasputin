var snoo = function(){};

var request = require('request');
var Entities = require('html-entities').XmlEntities;
var cheerio = require('cheerio');
var cheerioTableParser = require('cheerio-tableparser');

var entities = new Entities();
snoo.prototype.getXurItems = function(callback){
    request('http://www.reddit.com/r/DestinyTheGame.json', (err, res, body) => {
        var frontPageJSON = JSON.parse(body);
        frontPageJSON.data.children.forEach(entry => {
            var title = entry.data.title;
            if(title.indexOf('[D2] Xûr Megathread') != -1){
                var selfText = entry.data.selftext_html;
                var decoded = entities.decode(selfText);
                var tableText = '<table id="items">'+decoded.split('<table>')[1].split('</table>')[0]+"</table>";
                var table = cheerio.load(tableText);

                var consumablesTableText = '<table id="citems">'+decoded.split('<table>')[2].split('</table>')[0]+"</table>";
                var consumablesTable = cheerio.load(consumablesTableText);

                cheerioTableParser(table);
                var data = table("#items").parsetable(true, true, true);
                data[0].splice(0, 1);
                data[1].splice(0, 1);
                data[2].splice(0, 1);
                var items = data[0] 
                var costs = data[2];

                var itemsCosts = [];
                for(var i = 0; i < items.length; i++){
                    var item = {name: items[i], cost: costs[i]};
                    itemsCosts.push(item);
                }


                cheerioTableParser(consumablesTable);
                var cdata = consumablesTable("#citems").parsetable(true, true, true);
                cdata[0].splice(0, 1);
                cdata[1].splice(0, 1);
                cdata[2].splice(0, 1);
                console.log(cdata);
                
                var citems = cdata[0] 
                var ccosts = cdata[2];
                
                var citemsCosts = [];
                for(var i = 0; i < citems.length; i++){
                    var item = { name: citems[i], cost: ccosts[i]};
                    itemsCosts.push(item);
                }
                
                callback(itemsCosts);
            }
        });
    });
}

snoo.prototype.getXurLocation = function(callback) {
    request('http://www.reddit.com/r/DestinyTheGame.json', (err, res, body) => {
        var frontPageJSON = JSON.parse(body);
        frontPageJSON.data.children.forEach(entry => {
            var title = entry.data.title;
            if(title.indexOf('[D2] Xûr Megathread') != -1){
                var selfText = entry.data.selftext_html;
                var decoded = entities.decode(selfText);
                callback(decoded.split("<p><strong>Location:</strong></p>\n\n<p><em>")[1].split("</em></p>")[0]);
            }
        });
    });
}

module.exports = snoo;
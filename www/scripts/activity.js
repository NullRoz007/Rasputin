function findGetParameter(parameterName) {
    var result = null 
    var tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] == parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}

$(document).ready(() => {
    var hash = findGetParameter('hash');
    var type = findGetParameter('type');

    console.log("Loading: "+hash);
    $.getJSON('https://destiny.plumbing/en/raw/DestinyActivityDefinition.json', (destinyActivities) => {
            for(i in destinyActivities){
                 
                if(destinyActivities[i].hash == hash) {
                    var activity = destinyActivities[i];
                    console.log(activity);
                    $('#activity-name').html('<b>'+activity.displayProperties.name.toUpperCase());
                    $('#activity-desc').html("<i>"+activity.displayProperties.description+"</i>");

                    $('#loading-image').html("<img src=\"https://www.bungie.net"+activity.pgcrImage+"\">")
                    $('#loading-image').css("margin"," 0% auto");
                    $('#loading-image').css("width","1280px");

                    if(activity.rewards.length == 0) $("#rewards").text("| Rewards: Unknown");
                    else {
                        $.getJSON('https://destiny.plumbing/en/raw/DestinyInventoryItemDefinition.json', (destinyItems) => {
                            console.log("!");
                            $("#rewards").text("| Rewards:");
                            for(var ri in activity.rewards){
                                var rewardItems = activity.rewards[ri].rewardItems;
                                for(var rh in rewardItems){
                                    var rewardHash = rewardItems[rh].itemHash;
                                    console.log(rewardHash);
                                    for(i in destinyItems){
                                        if(destinyItems[i].hash == rewardHash){
                                            $("#rewards").append("<br><img src=\"http://www.bungie.net"+destinyItems[i].displayProperties.icon+"\"> "+destinyItems[i].displayProperties.name)
                                        }
                                    }
                                }
                            }
                        });
                    }
                }
            }
        
    });

});
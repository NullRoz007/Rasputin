function findGetParameter(parameterName) {
    var result = null 
    var tmp = [];
    var items = location.search.substr(1).split("&");
    for (var index = 0; index < items.length; index++) {
        tmp = items[index].split("=");
        if (tmp[0] == parameterName) result = decodeURIComponent(tmp[1]);
    }
    return result;
}

$(document).ready(() => {

    var hash = findGetParameter('hash');
    var cost = findGetParameter('cost');

    console.log(cost);

    if(!hash) {
        console.log("No hash, aborting...");
    } else {
        console.log("Loading: ITEM "+hash);
        var apiKey = "cd4c44b66cc04c33962f7467bf844731";
        var item;
        $.getJSON('https://destiny.plumbing/en/raw/DestinyInventoryItemDefinition.json', (destinyInventoryItems) => {
            for(i in destinyInventoryItems){
                 
                if(destinyInventoryItems[i].hash == hash) {
                    item = destinyInventoryItems[i];
                    console.log(item);
                    var costText = (cost != null) ? ' <div id="item-cost"> '+cost+' LEGENDARY SHARDS </div>' : ''
                    $('#item-name').html('<b>'+item.displayProperties.name.toUpperCase()+'</b>'+costText);
                    $('#item-type').text(item.itemTypeDisplayName.toUpperCase());
                    if(item.itemType == 20) $('#item-type').text("PURCHASABLE");

                    $('#item-icon').attr('src', "https://bungie.net/"+item.displayProperties.icon);
                    $('#item-desc').html("<i>"+item.displayProperties.description+"</i>");
                    
                    
                    if(item.loreHash) {
                        $.getJSON('https://destiny.plumbing/en/raw/DestinyLoreDefinition.json', (loreDefintions) => {
                            var lore = loreDefintions[item.loreHash];
                            $('#item-lore').html("<hr><br>"+lore.displayProperties.description);
                        });
                    }
                    
                    if(item.screenshot) {
                        $('body').css('background-image', 'url("https://bungie.net/'+item.screenshot+'")');
                        $('#loading-image').css('opacity', 0);
                    } else {
                        $('body').css('background-image', 'url("https://www.bungie.net/img/theme/destiny/bgs/gradient_green.jpg")');
                        $('#loading-image').css('opacity', 0);
                    }

                    if(item.itemType == 2) {
                        console.log("ARMOR");
                        var resilience = item.stats.stats['392767087'];
                        var mobility = item.stats.stats['2996146975'];

                        $('#stat1').text("Resilience: "+resilience.value);
                        $('#stat2').text("Mobility: "+mobility.value);
                        

                    } else if(item.itemType == 3){
                        console.log("GUN");
                        var stability = item.stats.stats['155624089']; //stab
                        var magSize = item.stats.stats['3871231066']; //mag
                        var reload = item.stats.stats['4188031367']; //reload
                        var rpm = item.stats.stats['4284893193'];
                        
                        $('#stat1').text("Stability: "+stability.value);
                        $('#stat2').text("Magazine: "+magSize.value);
                        $('#stat3').text("Reload Speed: "+reload.value);

                    } else {
                        console.log("!");
                        $('#loading-image').css('opacity', 0);
                        $('#item-type').css('color', 'white');
                        $('#item-desc').css('color', 'white');
						$('#stats').css('opacity', 0);
                        
                    }
                    //stats
                    var powerHash = (item.itemType == 2) ? 3897883278 : 1480404414;
                    var power = item.stats.stats[powerHash].value;

                    var powerText = (item.itemType == 2) ? "DEFENSE" : "ATTACK";
                    $('#item-power').html(power);
                    $('#item-power-text').html(powerText);
                }
            }
        });
    }    
});
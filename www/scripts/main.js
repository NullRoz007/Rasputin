function attachHandlers(){
    /*
    Discord Dropdown
    */
    $('#discord-link').mouseover(() => {
        console.log("entered.");
        $('#discord-dropdown').fadeTo(200, 1) 
    });

    $('#discord-link').mouseleave(() => {
        console.log("left")
        $('#discord-dropdown').fadeTo(200, 0);
    });

    /*
    Bungie Dropdown
    */
    $('#bungie-link').mouseover(() => {
        console.log("entered.");
        $('#bungie-dropdown').fadeTo(200, 1) 
    });

    $('#bungie-link').mouseleave(() => {
        console.log("left")
        $('#bungie-dropdown').fadeTo(200, 0);
    });
}

$(document).ready(() => {
    try {
        /*
        Twitch
        */
        var twitchUserName = "seraphimelite1";
        
        $.ajax({
            url: "https://api.twitch.tv/kraken/streams/"+twitchUserName,
            type: 'GET',
            dataType: 'json',
            success: (streamData) => {
                console.log('Stream Data:', streamData) 
                if(streamData && streamData.stream) {
                    $('#stream-widget').html("<b class='online'>Twitch: ONLINE</b> twitch.tv: <a href='http://www.twitch.tv/"+twitchUserName+"'>"+ streamData.stream.channel.status+"</a>")
                } else {
                    $('#stream-widget').html("<b class='offline'>Twitch: OFFLINE</b>")
                }
            },
            error: (err) => {
                console.log(err);
            },
            beforeSend: (xhr) => {
                xhr.setRequestHeader('Client-ID', '4nl43m7wvaltcly6fsm921811950ij');
            }
        });
    
        
    
        attachHandlers();
    }
    catch (err) {
        console.log(err);
    }
    /*
    RAS
    */
    $.getJSON('/api/status', (result) => {
        var statusText;
        console.log(result);
        if(result.status == 'offline'){
            statusText = 'Rasputin is <span class="red">OFFLINE</span>, please contact a Bot Commander on Discord.'
        } else {
            statusText = 'Rasputin is <span class="green">ONLINE</span>'
        }
        document.getElementById('ras-status').innerHTML = statusText;
        
        $.getJSON('/api/clanmembers', (result) => {
            if(result.status != 'offline'){
                document.getElementById('clan-members').innerText = "Clan Members: "+result.data;
            }
        })
    })
});

function setupWelcomeMessage() {
    $.getJSON('/api/welcomeimage', (result) => {
        console.log(result);
        $('#preview').attr('src', result.data);
    });

    $.get('/api/welcomemessage', result => {
        document.getElementById('welcome-message-text').innerHTML = result;
    });
}

function updateWelcomeMessage() {
    console.log(document.getElementById('x-text').value);
    console.log(document.getElementById('y-text').value);
    $.post('/api/private/setwelcomemessage', {
        username: document.getElementById('username').value,
        password: document.getElementById('password').value,
        text: document.getElementById('welcome-message-text').value,
        x: document.getElementById('x-text').value,
        y: document.getElementById('y-text').value
    }, (data, status) => {
        console.log(data);
    });

    setupWelcomeMessage();
}

function setupGroups() {
    $.getJSON('/api/groups', (result) => {
        if(result.status != 'offline'){
            for(var i = 0; i < result.length; i++){
                var serverGroups = result[i];
                for(var gn = 0; gn < serverGroups.length; gn++){
                    var group = serverGroups[gn];
                    var html = '<li>'+group.name+' : <span id="detail">'+group.date + ' '+group.startTime+group.timeZone+'</span></li>'
                    $("#groups-list").append(html);
                }
            }
        }
        /*<li id="group-list-item ">
        Test Group : 
        <span id="detail">
            10:00PM - 22nd Feb  
        </span>
        </li>*/
        
    });
}